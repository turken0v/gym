from queue import Queue


class PriorityQueue(Queue):
    def get(self) -> any:
        assert self.not_empty, 'Priority Queue is empty'

        max_priority = self.queue[0][0]
        max_priority_idx = 0
        for i, element in enumerate(self.queue):
            value = element[0]
            if value > max_priority:
                max_priority = value
                max_priority_idx = i

        value = self.queue[max_priority_idx][1]
        del self.queue[max_priority_idx]
        return value


if __name__ == '__main__':
    pq = PriorityQueue()
    pq.put((1, 1))
    pq.put((2, 2))
    print(pq.get())