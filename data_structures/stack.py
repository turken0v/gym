class Stack(list):

    @property
    def top(self):
        return self[-1]

    def is_empty(self):
        return len(self) == 0


if __name__ == '__main__':
    s = Stack()
    s.append(1)
    print(s.pop())