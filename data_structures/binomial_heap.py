class BinomialTree:
    def __init__(self, key):
        self.key = key
        self.children = []
        self.order = 0

    def append(self, child):
        self.children.append(child)
        self.order = self.order + 1

    def __lt__(self, other):
        if isinstance(other, BinomialTree):
            return self.key < other.key

        raise ValueError

    def __gt__(self, other):
        if isinstance(other, BinomialTree):
            return self.key > other.key

        raise ValueError
 
    def __repr__(self) -> str:
        if self.order > 0:
            return str(self.key) + str(self.children)

        return str(self.key)

class BinomialHeap:
    def __init__(self):
        self.trees = []

    def extract_min(self):
        if not self.trees:
            return None

        least = self.trees[0]
        for tree in self.trees:
            if least > tree:
                least = tree

        self.trees.remove(least)
        heap = BinomialHeap()
        heap.trees = least.children
        self.merge(heap)
 
        return least.key

    def get_min(self):
        if not self.trees:
            return None

        least = self.trees[0]
        for tree in self.trees:
            if least > tree:
                least = tree

        return least

    def combine_roots(self, h):
        self.trees.extend(h.trees)
        self.trees.sort(key=lambda tree: tree.order)

    def merge(self, h):
        self.combine_roots(h)
        if not self.trees:
            return None

        i = 0
        while len(self.trees)-1 > i:
            current = self.trees[i]
            after = self.trees[i+1]
            if current.order == after.order:
                if after > current:
                    current.append(after)
                    del self.trees[i+1]
                else:
                    after.append(current)
                    del self.trees[i]
            else:
                i += 1

    def insert(self, key):
        tree = BinomialTree(key)
        heap = BinomialHeap()
        heap.trees.append(tree)
        self.merge(heap)


if __name__ == '__main__':
    bheap = BinomialHeap()
    bheap.insert(3)
    bheap.insert(7)
    bheap.insert(1)
    bheap.insert(4)
    print(bheap.get_min())