from data_structures.binary_tree import Node
from depth_first_traversal import left_most

class Node(Node):
    rightThread: bool = False

    def __init__(self, value=None, left=None, right=None, rightThread=False):
        super().__init__(value, left, right)
        self.rightThread = rightThread


def inorder_traversal(root: Node):
    answer = []
    node = left_most(root)
    while node:
        answer.append(node.data)

        if node.rightThread:
            node = node.right
        else:
            node = left_most(node.right)

    return answer


if __name__ == '__main__':
    _1 = Node(1)
    _2 = Node(2)
    _3 = Node(3)
    _4 = Node(4)
    _5 = Node(5)
    _6 = Node(6)

    _1.left = _2
    _1.right = _3
    _2.left = _4
    _2.right = _5
    _3.left = _6

    _4.rightThread = True
    _4.right = _2
    _5.rightThread = True
    _5.right = _1
    _6.rightThread = True
    _6.right = _3
    print(inorder_traversal(_1))