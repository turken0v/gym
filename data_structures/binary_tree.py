class Node:
    data: any = None
    left = None
    right = None

    def __init__(self, value=None, left=None, right=None):
        self.data = value
        self.left = left
        self.right = right

    def __eq__(self, node) -> bool:
        if isinstance(node, Node):
            return self.data == node.data

        raise TypeError

    def __gt__(self, node) -> bool:
        if isinstance(node, Node):
            return self.data > node.data

        raise TypeError

    def __gte__(self, node) -> bool:
        if isinstance(node, Node):
            return self.data >= node.data

        raise TypeError

    def __lt__(self, node) -> bool:
        if isinstance(node, Node):
            return self.data < node.data

        raise TypeError

    def __lte__(self, node) -> bool:
        if isinstance(node, Node):
            return self.data <= node.data

        raise TypeError

    def are_identical(self):
        pass

    def __repr__(self) -> str:
        return str(self.data)


def print_inorder_binary_tree(root: Node) -> None:
    if isinstance(root.left, Node):
        print_inorder_binary_tree(root.left)

    print(root.data, end=' ')

    if isinstance(root.right, Node):
        print_inorder_binary_tree(root.right)


if __name__ == '__main__':
    _1 = Node(1)
    _2 = Node(1)

    print(_1 == _2)