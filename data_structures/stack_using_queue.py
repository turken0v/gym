from queue import Queue


if __name__ == '__main__':
    q = Queue(maxsize=10)
    q.put(1)
    q.put(2)
    q.put(3)
    q.put(4)

    size = q.qsize()
    for i in range(size-1):
        element = q.get()
        q.put(element)

    lifo = q.get()
    print(lifo)