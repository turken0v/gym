#include <iostream>
using namespace std;

class Queue
{
public:
    int front = -1,
        rear = -1,
        max_size;
    int* data;

    Queue(int max_size)
    {
        this->max_size = max_size;

        this->data = new int[
            (max_size * sizeof(int))
        ];
    }

    size_t size()
    {
        return this->front - this->rear;
    }


    bool is_full()
    {
        return this->size() == this->max_size;
    }


    bool is_empty()
    {
        return this->size() == 0;
    }


    void put(int value)
    {
        if (this->is_full())
        {
            throw "Queue is full";
        }

        this->front = (this->front + 1) % this->max_size;
        this->data[this->front] = value;
    }


    int get()
    {
        if (this->is_empty())
        {
            throw "Queue is empty";
        }

        this->rear = (this->rear + 1) % this->max_size;
        return this->data[this->rear];
    }
};


int main()
{
    Queue q(3);

    q.put(1);
    cout << q.get() << " " << q.rear << " " << q.front << endl;

    q.put(2);
    cout << q.get() << " " << q.rear << " " << q.front << endl;

    q.put(3);
    cout << q.get() << " " << q.rear << " " << q.front << endl;

    q.put(4);
    cout << q.get() << " " << q.rear << " " << q.front << endl;

    q.put(5);
    cout << q.get() << " " << q.rear << " " << q.front << endl;

    return 0;
}