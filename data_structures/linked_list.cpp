#include <iostream>
#include <boost/any.hpp>
#include <vector>
#include <functional>
#include <unordered_map>
#include <string>
using namespace std;
using namespace boost;


class Element
{
    void init()
    {
        this->next = nullptr;
        this->prev = nullptr;
        this->value = nullptr;
    }

    public:
    Element* next;
    Element* prev;
    any value;

    Element()
    {
        this->init();
    }

    ~Element()
    {

    }

    Element(any value)
    {
        this->init();
        this->value = value;
    }

    Element(const Element &e)
    {
        this->set_next(e.next);
        this->set_prev(e.prev);
        this->set_value(e.value);
    }

    void update_by_pointer(void * p)
    {
        Element e;
        memcpy(&e, p, sizeof(Element));
        this->set_next(e.next);
        this->set_prev(e.prev);
        this->set_value(e.value);
    }

    any get_value()
    {
        return this->value;
    }

    void set_value(any value)
    {
        this->value = value;
    }

    void set_prev(Element *e)
    {
        this->prev = e;
    }

    void unset_prev()
    {
        this->prev = nullptr;
    }

    void set_next(Element *e)
    {
        this->next = e;
    }

    void unset_next()
    {
        this->next = nullptr;
    }
};


class List
{
    long len = -1;

    public:

    Element * next(Element * e)
    {
        if (e == nullptr)
        {
            throw "";
        }
        return e->next;
    }

    Element * prev(Element * e)
    {
        if (e == nullptr)
        {
            throw "";
        }
        return e->prev;
    }

    Element * loop(size_t r)
    {
        Element *e = this->head;
        for (size_t i = 0; i < r; i++)
        {
            e = this->next(e);
        }
        return e;
    }

    Element * rloop(size_t l)
    {
        Element *e = this->ceil;
        float len = this->length();
        for (size_t i = len-1; i > l; i--)
        {
            e = this->prev(e);
        }
        return e;
    }

    Element * at(long i)
    {
        Element * e;
        float len = this->length() - 1;

        if (i < 0)
        {
            i += len + 1;
        }
        else if (i > len)
        {
            e->set_value(nullptr);
            return e;
        }

        float k = i / len;
        try {
            e = k >= .5 ? rloop(i) : loop(i);
        } catch (...) {
            throw "";
        };

        return e;
    }

    size_t get_mid_idx()
    {
        float len = this->length();
        float remainder = 0;
        if (len > 1)
        {
            remainder = this->length() % 2;
        }

        return len / 2 + remainder;
    }

    Element* head;
    Element* ceil;

    size_t length()
    {
        if (this->len < 0)
        {
            this->len = this->count_length();
        }

        return this->len;
    }

    size_t count_length()
    {
        size_t len = 0;
        try
        {
            while (true)
            {
                len++;
                this->loop(len);
            }
        }
        catch (...)
        {
            len--;
        }

        return len;
    }

    void reset_length()
    {
        this->len = -1;
    }

    any operator[] (long i)
    {
        Element * e = this->at(i);
        return e->get_value();
    }

    List()
    {
        this->head = nullptr;
        this->ceil = nullptr;
    }

    void append(any value)
    {
        bool empty = this->is_empty();

        Element *e = new Element(value);

        if (this->ceil != nullptr)
        {
            e->set_prev(this->ceil);
            this->ceil->set_next(e);
        }

        this->ceil = e;

        if (this->head == nullptr)
        {
            this->head = e;
        }

        this->len = this->count_length();
    }

    any pop(long i = -1)
    {
        Element * e;
        try {
            e = this->at(i);
        } catch (...) {
            throw "";
        };

        this->len--;

        return e->get_value();
    }

    void swap_elements(long a, long b)
    {
        if (this->length() < 2)
        {
            throw "";
        }

        Element *x = this->at(a);
        Element *y = this->at(b);

        Element *nb2 = x->next;
        Element *nb3 = y->prev;

        if (x->prev == nullptr)
        {
            y->unset_prev();
            this->head = y;
        }
        else
        {
            Element *nb1 = x->prev;
            nb1->set_next(y);
            y->set_prev(nb1);
        }

        if (y->next == nullptr)
        {
            x->unset_next();
            this->ceil = x;
        }
        else
        {
            Element *nb4 = y->next;
            nb4->set_prev(x);
            x->set_next(nb4);
        }

        nb2->set_prev(y);
        y->set_next(nb2);
        nb3->set_next(x);
        x->set_prev(nb3);
    }

    void reverse()
    {
        size_t j;
        float len = this->length();
        for (size_t i = 0; i < len / 2; i++)
        {
            j = len - 1 - i;
            this->swap_elements(i, j);
        }
    }

    List divide()
    {
        List right;
        if (this->length() < 2)
        {
            return right;
        }

        size_t mid_idx = this->get_mid_idx();

        Element *mid = this->at(mid_idx);
        Element *prev = mid->prev;

        right.ceil = this->ceil;

        prev->unset_next();
        this->ceil = prev;

        mid->unset_prev();
        right.head = mid;

        this->reset_length();

        return right;
    }

    bool is_empty()
    {
        return this->length() == 0;
    }
};


bool gte(const boost::any& lhs, const boost::any& rhs)
{
    if (lhs.type() != rhs.type())
        return false;

    if (lhs.type() == typeid(int))
        return any_cast<int>(lhs) >= any_cast<int>(rhs);

    if (lhs.type() == typeid(short))
        return any_cast<short>(lhs) >= any_cast<short>(rhs);

    if (lhs.type() == typeid(long))
        return any_cast<long>(lhs) >= any_cast<long>(rhs);

    if (lhs.type() == typeid(double))
        return any_cast<double>(lhs) >= any_cast<double>(rhs);

    if (lhs.type() == typeid(float))
        return any_cast<float>(lhs) >= any_cast<float>(rhs);

    return false;
}


List merge(List a, List b)
{
    size_t
        alen = a.length(),
        blen = b.length(),
        ai = 0,
        bi = 0;
    List merged;
    while (alen + blen > ai + bi)
    {
        if (ai >= alen)
        {
            merged.append(b[bi]);
            bi++;
            continue;
        }

        if (bi >= blen)
        {
            merged.append(a[ai]);
            ai++;
            continue;
        }

        if (gte(b[bi], a[ai]))
        {
            merged.append(a[ai]);
            ai++;
        }
        else
        {
            merged.append(b[bi]);
            bi++;
        }
    }

    return merged;
}


List merge_sort(List a)
{
    if (a.length() < 2)
    {
        return a;
    }

    List b = a.divide();
    if (b.is_empty())
    {
        return a;
    }

    List left = merge_sort(a);
    List right = merge_sort(b);

    List sorted = merge(left, right);
    return sorted;
}


void print_list(List a)
{
    size_t asize = a.length();
    cout << endl << "size: " << asize << "\t";
    for (size_t i = 0; i < asize; i++)
    {
        any value = a[i];
        cout << " " << any_cast <int> (value);
    }
    cout << endl;
}


bool detect_loop(List A)
{
    unordered_map<void *, bool> M;

    Element *e;
    size_t len = A.length();
    for (size_t i = 0; i < len; i++)
    {
        e = A.at(i);

        if (M[e->next] == true)
        {
            return true;
        }

        M[e] = true;
    }

    return false;
}


List represent_by_lists(List A, List B)
{
    string S;
    size_t len = A.length();
    for (size_t i = 0; i < len; i++)
    {
        S += to_string(any_cast <int> (A[i]));
    }
    int value = stoi(S);

    S = "";
    len = B.length();
    for (size_t i = 0; i < len; i++)
    {
        S += to_string(any_cast <int> (B[i]));
    }
    value += stoi(S);

    List R;
    S = to_string(value);
    len = S.size();
    string num;
    for (int i = 0; i < len; i++)
    {
        num = S[i];
        value = stoi(num);
        R.append(value);
    }

    return R;
}


List rotate_list(List A, size_t d = 1)
{
    List R;
    size_t len = A.length();
    size_t r = len + d;
    size_t j = d;
    for (size_t i = 0; i < len; i++, j++)
    {
        if (j >= len)
        {
            j = 0;
        }

        R.append(A[j]);
    }

    return R;
}


int main()
{
    List c;

    c.append(1);
    c.append(4);
    c.append(9);
    c.append(10);
    c.append(5);
    c.append(6);
    c.append(2);
    c.append(7);
    c.append(3);
    c.append(8);

    print_list(c);
    List a = rotate_list(c, 2);
    print_list(a);

    return 0;
}