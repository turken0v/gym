from collections import deque


# Double Ended Queue
class Deque(deque):

    @property
    def front(self) -> any:
        return self[-1]

    @property
    def rear(self) -> any:
        return self[0]

    def is_empty(self) -> bool:
        return self.__len__() == 0


if __name__ == '__main__':
    deq = Deque()

    deq.appendleft(1)
    print(deq.front)
    deq.popleft()