from data_structures.binary_tree import Node, print_inorder_binary_tree
from data_structures.threaded_binary_tree import left_most
from data_structures.stack import Stack
from depth_first_traversal import inorder_traversal


def bst_search(root: Node, value) -> Node:
    if root.data == value:
        return root
    elif root.data > value:
        if isinstance(root.left, Node):
            return bst_search(root.left, value)
    else:
        if isinstance(root.right, Node):
            return bst_search(root.right, value)


def bst_insert(root: Node, value) -> None:
    if root.data > value:
        if isinstance(root.left, Node):
            bst_insert(root.left, value)
        else:
            root.left = Node(value)
    elif value > root.data:
        if isinstance(root.right, Node):
            bst_insert(root.right, value)
        else:
            root.right = Node(value)


def get_inorder_successor_with_parent(root: Node):
    node = root
    parent = None
    while node and node.left is not None:
        parent = node
        node = node.left

    return node, parent

def bst_delete(node: Node, parent: Node = Node) -> None:
    if isinstance(node.right, Node):
        successor, successor_parent = get_inorder_successor_with_parent(node.right)
        node.data = successor.data
        bst_delete(successor, successor_parent)
    elif isinstance(node.left, Node):
        node.data = node.left.data
        node.left = None
    else:
        if parent.left == node:
            parent.left = None
        else:
            parent.right = None


# def is_bst(root: Node) -> bool:
#     if root is None:
#         return False

#     buffer = Stack([root])
#     while buffer:
#         node = buffer.pop()

#         if isinstance(node.right, Node):
#             if node > node.right:
#                 return False

#             for selement in buffer:
#                 if node.right > selement:
#                     return False

#             buffer.append(node.right)

#         if isinstance(node.left, Node):
#             if node.left > node:
#                 return False

#             for selement in buffer:
#                 if node.left > selement:
#                     return False

#             buffer.append(node.left)

#     return True


# def is_bst(root: Node):
#     nodes = inorder_traversal(root)

#     size = len(nodes)
#     for i in range(1, size):
#         predecessor = nodes[i-1]
#         node = nodes[i]

#         if predecessor > node:
#             return False

#     return True


def _is_bst(root: Node):
    if root is None:
        return False

    if isinstance(root.left, Node):
        if not _is_bst(root.left):
            return False

    if isinstance(_is_bst.prev, Node) and _is_bst.prev > root:
        return False

    _is_bst.prev = root

    if isinstance(root.right, Node):
        if not _is_bst(root.right):
            return False

    return True


def is_bst(root: Node) -> bool:
    setattr(_is_bst, 'prev', None)
    return _is_bst(root)


if __name__ == '__main__':
    # _1 = Node(1)
    # _3 = Node(3)
    # _4 = Node(4)
    # _6 = Node(6)
    # _7 = Node(7)
    # _8 = Node(8)
    # _10 = Node(10)
    # _13 = Node(13)
    # _14 = Node(14)

    # _8.left = _3
    # _8.right = _10
    # _3.left = _1
    # _3.right = _6
    # _6.left = _4
    # _6.right = _7
    # _10.right = _14
    # _14.left = _13

    # bst_insert(_8, 5)
    # node = bst_search(_8, 5)
    # bst_delete(_8)

    # print_inorder_binary_tree(_8)
    # print()

    # # Min value
    # print(left_most(_8))

    _1 = Node(1)
    _2 = Node(2)
    _3 = Node(3)
    _4 = Node(4)
    _5 = Node(5)
    _3.left = _2
    _3.right = _5
    _2.right = _4
    # _4.left = _2
    # _4.right = _5
    # _2.right = _3
    _2.left = _1
    print_inorder_binary_tree(_3)
    print(is_bst(_3))