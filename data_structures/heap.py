from typing import List


def fix_heap(elements: List, i = None) -> int:
    if i is None:
        i = len(elements) - 1

    if i == 0:
        return None

    pi = i // 2 - (1 if i % 2 == 0 and i != 0 else 0)
    if elements[i] > elements[pi]:
        elements[i], elements[pi] = elements[pi], elements[i]
        fix_heap(elements, pi)
        fix_heap(elements, i)

    return i


def heapify(elements: List, i = None) -> None:
    i = fix_heap(elements, i)
    if i is None:
        return None

    heapify(elements, i-1)


def fix_heap_after_pop(elements: List, size = None, i = 0) -> None:
    if size is None:
        size = len(elements)

    r = (i+1)*2
    l = r-1
    if l >= size-1:
        return None

    largest = r if elements[r] > elements[l] else l

    if elements[largest] > elements[i]:
        elements[largest], elements[i] = elements[i], elements[largest]
        fix_heap_after_pop(elements, size, largest)


def hpop(elements: List) -> any:
    required = elements[0]
    elements[0] = elements[-1]
    elements.pop()
    fix_heap_after_pop(elements)
    return required


def hpush(elements: List, value) -> None:
    elements.append(value)
    fix_heap(elements)


if __name__ == '__main__':
    a = [0,1,2,3,4,5,6,7,8]
    heapify(a)
    print(a)
    a = [20,15,9,6,11,8,5,4,3,7]
    print(a)
    hpush(a, 24)
    print(a)
