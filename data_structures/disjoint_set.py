class DisjointNode:
    key: any
    parent: any = None
    rank: int = 0

    def __init__(self, key: any, parent: any = None, rank: int = 0):
        self.key = key
        self.parent = parent
        self.rank = rank


def find_parent(nodes: dict, key: any):
    node: DisjointNode = nodes[key]
    if node.parent == None:
        return node.key

    return find_parent(nodes, node.parent)


def union(nodes: dict, ukey: any, vkey: any):
    unode = nodes[ukey]
    vnode = nodes[vkey]
    if unode.rank > vnode.rank:
        vnode.parent = ukey
    elif vnode.rank > unode.rank:
        unode.parent = vkey
    else:
        vnode.parent = ukey
        unode.rank += 1


# if __name__ == '__main__':
#     Node
#     Node
#     Node