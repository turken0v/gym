#include <iostream>
#include <vector>
using namespace std;


// int unique_paths(int m, int n)
// {
//     if (m < 1 || n < 1)
//     {
//         return 0;
//     }

//     if (m == 1 && n == 1)
//     {
//         return 1;
//     }

//     return unique_paths(m - 1, n) + unique_paths(m, n - 1);
// }


int unique_paths(int m, int n)
{
    int board[m][n];
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            board[i][j] = 0;
        }
    }

    for (int i = 0; i < m-1; i++)
    {
        for (int j = 0; j < n-1; j++)
        {
            board[i][j+1] += 1;
            board[i+1][j] += 1;
        }
    }

    int answer = 0;
    for (int i = 0; i < m; i++)
    {
        for (int j = 0; j < n; j++)
        {
            answer += board[i][j];
            cout << board[i][j] << ' ';
        }
        cout << endl;
    }

    return answer;
}


int main()
{
    cout << unique_paths(3, 7) << endl; // 28
    cout << unique_paths(3, 2) << endl; // 3
    cout << unique_paths(3, 3) << endl; // 6
    // cout << unique_paths(7, 3) << endl;
    // cout << unique_paths(3, 4) << endl;
    return 0;
}