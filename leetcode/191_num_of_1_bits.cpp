#include <iostream>
#include <string>
using namespace std;


int hamming_weight(uint32_t n)
{
    int answer = 0;
    while (n > 0)
    {
        if (n % 2 == 1)
        {
            answer += 1;
        }

        n >>= 1;
    }

    return answer;
}


int main()
{
    cout << hamming_weight(4294967294) << endl;

    return 0;
}