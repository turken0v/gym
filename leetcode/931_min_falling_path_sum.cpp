#include <iostream>
#include <vector>
using namespace std;


int min_falling_path_sum(vector<vector<int>>& matrix)
{
    vector<vector<int>> dp = matrix;

    int n = matrix.size(), m;
    for (int i = 1; i < n; i++)
    {
        m = matrix[i-1].size();
        for (int j = 0; j < matrix[i].size(); j++)
        {
            dp[i][j] = matrix[i][j] + dp[i-1][j];

            if (j-1 >= 0)
            {
                dp[i][j] = min(dp[i][j], matrix[i][j] + dp[i-1][j-1]);
            }

            if (m > j+1)
            {
                dp[i][j] = min(dp[i][j], matrix[i][j] + dp[i-1][j+1]);
            }
        }
    }

    int smallest = dp[n-1][0];
    for (int i = 1; i < matrix[n-1].size(); i++)
    {
        if (smallest > dp[n-1][i])
        {
            smallest = dp[n-1][i];
        }
    }

    return smallest;
}


int main()
{
    vector<vector<int>> matrix {
        {2,1,3},
        {6,5,4},
        {7,8,9}
    };

    cout << min_falling_path_sum(matrix) << endl;

    return 0;
}