#include <iostream>
#include <string>
#include <map>
using namespace std;


int first_unique_char(string input)
{
    int n = input.length();
    map<char, int> dictionary;
    for (int i = 0; i < n; i++)
    {
        if (dictionary[input[i]] == 0)
        {
            dictionary[input[i]] = i + 1;
        }
        else
        {
            dictionary[input[i]] = -1;
        }
    }

    int index = n+1;
    for(const pair<char, int>& it: dictionary) {
        if (it.second > 0 && index > it.second)
        {
            index = it.second;
        }
    }

    if (n > 1 && index == n+1)
    {
        return -1;
    }

    return index-1;
}


int main()
{
    cout << first_unique_char("leetcode") << endl;

    return 0;
}