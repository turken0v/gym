#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;


int max_subarray(vector<int>& nums)
{
    int n = nums.size(), value;
    vector<int> dp = nums;
    for (int i = 1; i < n; i++)
    {
        value = dp[i-1] + dp[i];
        dp[i] = max(value, nums[i]);
    }

    int largest = dp[0];
    for (int i = 1; i < n; i++)
    {
        if (dp[i] > largest)
        {
            largest = dp[i];
        }
    }

    return largest;
}


int main()
{
    vector<int> input {-2,1,-3,4,-1,2,1,-5,4};
    cout << max_subarray(input) << endl;

    input = {1};
    cout << max_subarray(input) << endl;

    input = {5,4,-1,7,8};
    cout << max_subarray(input) << endl;

    return 0;
}