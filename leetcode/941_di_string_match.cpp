#include <iostream>
#include <vector>
using namespace std;


vector<int> di_string_match(string input)
{
    int n = input.size(), l = 0, r = n;
    vector<int> output;
    for (int i = 0; i < n+1; i++)
    {
        if (input[i] == 'I')
        {
            output.push_back(l);
            l++;
        }
        else
        {
            output.push_back(r);
            r--;
        }
    }

    return output;
}


int main()
{
    vector<int> output;
    output = di_string_match("IDID");
    for (int i = 0; i < output.size(); i++)
    {
        cout << ' ' << output[i];
    }
    cout << endl;

    output = di_string_match("III");
    for (int i = 0; i < output.size(); i++)
    {
        cout << ' ' << output[i];
    }
    cout << endl;

    output = di_string_match("DDI");
    for (int i = 0; i < output.size(); i++)
    {
        cout << ' ' << output[i];
    }
    cout << endl;

    return 0;
}