#include <iostream>
#include <vector>
using namespace std;


vector<int> get_pascal_row(int n)
{
    vector<vector<int>> triangle { {1} };
    vector<int> row;
    for (int i = 1; i < n+1; i++)
    {
        row = {};

        row.push_back(1);
        for (int j = 1; j < i; j++)
        {
            row.push_back(triangle[i-1][j-1] + triangle[i-1][j]);
        }
        row.push_back(1);

        triangle.push_back(row);
    }

    return triangle[n];
}


int main()
{
    vector<int> output = get_pascal_row(4);
    for (int i = 0; i < output.size(); i++)
    {
        cout << output[i] << ' ';
    }

    return 0;
}