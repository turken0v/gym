from bisect import bisect_right
from typing import List


def advantage_shuffle(A: List, B: List) -> List:
    A = sorted(A)
    answer = []
    for u in B:
        s = bisect_right(A, u)
        
        if s >= len(A):
            s = 0

        v = A.pop(s)
        answer.append(v)

    return answer


if __name__ == '__main__':
    A = [12,24,8,32]
    B = [13,25,32,11]
    print(advantage_shuffle(A, B))