#include <iostream>
#include <vector>
#include <set>
using namespace std;


vector<int> separate2digits(int num)
{
    vector<int> answer;
    int digit;
    while (num != 0)
    {
        digit = num % 10;
        num = (num - digit) / 10;
        answer.push_back(digit);
    }

    return answer;
}


bool _is_happy_number(int num, set<int>& explored)
{
    if (num == 1)
    {
        return true;
    }

    if (explored.find(num) != explored.end())
    {
        return false;
    }

    explored.insert(num);

    vector<int> digits = separate2digits(num);

    num = 0;
    for (int i = 0; i < digits.size(); i++)
    {
        num += digits[i] * digits[i];
    }

    return _is_happy_number(num, explored);
}


bool is_happy_number(int num)
{
    set<int> explored;
    return _is_happy_number(num, explored);
}


int main()
{
    cout << is_happy_number(19) << endl << endl;
    cout << is_happy_number(10) << endl << endl;
    cout << is_happy_number(2) << endl << endl;
    cout << is_happy_number(3) << endl << endl;
    cout << is_happy_number(4) << endl << endl;
    cout << is_happy_number(5) << endl << endl;
    cout << is_happy_number(6) << endl << endl;
    cout << is_happy_number(7) << endl << endl;
    cout << is_happy_number(8) << endl << endl;

    return 0;
}