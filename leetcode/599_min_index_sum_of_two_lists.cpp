#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
using namespace std;


vector<string> find_restaurant(vector<string>& first, vector<string>& second)
{
    unordered_map<string, int> first_map;
    for (int i = 0; i < first.size(); i++)
    {
        first_map[first[i]] = i;
    }

    map<int, string> intersection;
    for (int i = 0; i < second.size(); i++)
    {
        auto it = first_map.find(second[i]);
        if (it == first_map.end())
        {
            continue;
        }

        intersection[it->second+i] = it->first;
    }

    vector<string> answer;
    for (auto it = intersection.begin(); it != intersection.end(); ++it)
    {
        answer.push_back(it->second);
    }

    return answer;
}


int main()
{
    vector<string> first {"Shogun","Tapioca Express","Burger King","KFC"},
        second {"KFC", "Burger King", "Tapioca Express", "Shogun"},
        output;

    output = find_restaurant(first, second);

    for (int i = 0; i < output.size(); i++)
    {
        cout << output[i] << ' ';
    }

    cout << endl;

    return 0;
}