#include <iostream>
#include <vector>
using namespace std;


vector<int> two_sum(vector<int>& nums, int target)
{
    vector<int> answer;
    int n = nums.size(), value, i = 0, j = n - 1;
    while (j > i)
    {
        value = target - (nums[i] + nums[j]);

        if (value == 0)
        {
            return {i, j};
        }

        if (value > 0)
        {
            i++;
        }
        else
        {
            j--;            
        }
    }

    return answer;
}


int main()
{
    vector<int> input {2,7,11,15};
    vector<int> output = two_sum(input, 9);

    for (int i = 0; i < output.size(); i++)
    {
        cout << output[i] << ' ';
    }

    cout << endl;

    input = {2,3,4};
    output = two_sum(input, 6);

    for (int i = 0; i < output.size(); i++)
    {
        cout << output[i] << ' ';
    }

    cout << endl;

    return 0;
}