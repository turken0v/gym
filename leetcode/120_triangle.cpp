#include <iostream>
#include <functional>
#include <queue>
#include <utility>
#include <cmath>
#include <vector>
#include <algorithm>
using namespace std;


// int minimumTotal(vector<vector<int>>& triangle)
// {
// 	int min_weight = -2147483647;
// 	int n = triangle.size();

// 	priority_queue<pair<int, vector<pair<int, int>>>> heap;
// 	heap.push({
// 		-triangle[0][0],
// 		{ {0, 0} }
// 	});

// 	while (!heap.empty())
// 	{
// 		int weight;
// 		vector<pair<int, int>> path;
// 		tie(weight, path) = heap.top();
// 		heap.pop();

// 		int u, v;
// 		tie(u, v) = path.back();
// 		u += 1;

// 		if (u == n && weight > min_weight) {
// 			min_weight = weight;
// 		}

// 		if (n > u)
// 		{
// 			int m = triangle[u].size();
// 			vector<pair<int, int>> new_path;

// 			if (m > v && v >= 0)
// 			{
// 				new_path = path;
// 				new_path.push_back({u, v});
// 				heap.push({
// 					weight - triangle[u][v],
// 					new_path
// 				});
// 			}

// 			v += 1;
// 			if (m > v && v >= 0)
// 			{
// 				new_path = path;
// 				new_path.push_back({u, v});
// 				heap.push({
// 					weight - triangle[u][v],
// 					new_path
// 				});
// 			}
// 		}
// 	}

// 	return -min_weight;
// }


int minimumTotal(vector<vector<int>>& triangle)
{
	vector<int> first { triangle[0][0] };
	int n;
	for (int i = 1; i < triangle.size(); i++)
	{
		n = triangle[i].size();

		int second[n];
		for (int j = 0; j < n; j++)
		{
			second[j] = 2147483647;
		}

		for (int j = 0; j < first.size(); j++)
		{
			second[j] = min(second[j], first[j] + triangle[i][j]);
			second[j+1] = min(second[j+1], first[j] + triangle[i][j+1]);
		}

		first.clear();
		for (int j = 0; j < n; j++)
		{
			first.push_back(second[j]);
			cout << second[j] << ' ';
		}
	}

	int smallest = first[0];
	for (int i = 0; i < first.size(); i++)
	{
		if (smallest > first[i])
		{
			smallest = first[i];
		}
	}

	return smallest;
}


int main()
{
	vector<vector<int>> input {
		{2},
		{3,4},
		{6,5,7},
		{4,1,8,3}
	};

	cout << minimumTotal(input) << endl << endl;

	input = {
		{-1},
		{2,3},
		{1,-1,-3}
	};

	cout << minimumTotal(input) << endl << endl;

	input = {
		{2},
		{3,4},
		{6,5,9},
		{4,4,8,0}
	};

	cout << minimumTotal(input) << endl << endl;

	return 0;
}
