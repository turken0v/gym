#include <iostream>
using namespace std;


bool is_power_of_two(int n)
{
    int c = 0;
    while (n > 0)
    {
        if (n % 2 == 1)
        {
            c++;
        }

        n >>= 1;
    }

    if (c == 0 || c > 1)
    {
        return false;
    }

    return true;
}


int main()
{
    cout << is_power_of_two(16) << endl;
    cout << is_power_of_two(17) << endl;

    return 0;
}