#include <iostream>
using namespace std;


int count_vowel_strings(int n)
{
    int vowel[5];
    for (int i = 0; i < 5; i++)
    {
        vowel[i] = 1;
    }

    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            for (int k = j+1; k < 5; k++)
            {
                vowel[j] += vowel[k];
            }
        }
    }

    int answer = 0;
    for (int i = 0; i < 5; i++)
    {
        answer += vowel[i];
    }

    return answer;
}


int main()
{
    cout << count_vowel_strings(1) << endl;
    cout << count_vowel_strings(2) << endl;
    cout << count_vowel_strings(3) << endl;
    cout << count_vowel_strings(33) << endl;

    return 0;
}