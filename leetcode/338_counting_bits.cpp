#include <vector>
#include <iostream>
using namespace std;


vector<int> count_bits(int num)
{
    vector<int> dp {0};
    for (int i = 1, j; i <= num; i++)
    {
        j = i >> 1;
        dp.push_back(dp[j] + i % 2);
    }

    return dp;
}


int main()
{
    vector<int> output = count_bits(16);
    for (int i = 0; i < output.size(); i++)
    {
        cout << output[i] << ' ';
    }
    cout << endl;

    return 0;
}