#include <bits/stdc++.h>  
using namespace std;


struct abs_greater {
    bool operator()(const long& a,const long& b) const {
        return abs(a) < abs(b);
    }
};


int max_product(vector<int>& nums)
{
    vector<int> sorted = nums;
    make_heap(sorted.begin(), sorted.end(), abs_greater());

    int c = 1, temp;

    pop_heap(sorted.begin(), sorted.end(), abs_greater());
    c *= sorted.back();
    sorted.pop_back();

    pop_heap(sorted.begin(), sorted.end(), abs_greater());
    c *= sorted.back();
    sorted.pop_back();

    temp = c;
    do
    {
        pop_heap(sorted.begin(), sorted.end(), abs_greater());
        c *= sorted.back();
        sorted.pop_back();

        c = max(c, temp);
    }
    while (c < 0 && sorted.size() > 0);

    return c;
}


int main()
{
    vector<int> input = {1,2,3,4};
    cout << max_product(input) << endl;

    input = {-100,-98,-1,2,3,4};
    cout << max_product(input) << endl;

    return 0;
}