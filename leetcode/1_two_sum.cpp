#include <iostream>
#include <vector>
#include <map>
using namespace std;


vector<int> two_sum(vector<int>& nums, int target)
{
    vector<int> answer;
    int n = nums.size(), key;
    multimap<int, int> mapped;
    for (int i = 0; i < n; i++)
    {
        mapped.insert(
            pair<int, int> (nums[i], i)
        );
    }

    for (int i = 0; i < n; i++)
    {
        key = target - nums[i];

        auto it = mapped.lower_bound(key);
        auto end = mapped.upper_bound(key);

        for (; it != end; ++it)
        {
            if (it->second != i)
            {
                return {i, it->second};
            }
        }
    }

    return answer;
}


int main()
{
    vector<int> input {2,7,11,5};
    vector<int> output = two_sum(input, 9);

    for (int i = 0; i < output.size(); i++)
    {
        cout << output[i] << ' ';
    }

    cout << endl;

    input = {3,2,4};
    output = two_sum(input, 6);

    for (int i = 0; i < output.size(); i++)
    {
        cout << output[i] << ' ';
    }

    cout << endl;

    input = {3,3};
    output = two_sum(input, 6);

    for (int i = 0; i < output.size(); i++)
    {
        cout << output[i] << ' ';
    }

    cout << endl;

    return 0;
}