#include <iostream>
#include <vector>
using namespace std;


void duplicate_zeros(vector<int>& nums)
{
    for (int i = 0; i < nums.size(); i++)
    {
        if (nums[i] == 0)
        {
            nums.insert(nums.begin()+i, 0);
            nums.pop_back();

            i++;
        }
    }
}


int main()
{
    vector<int> input {1,0,2,3,0,4,5,0};

    duplicate_zeros(input);

    for (int i = 0; i < input.size(); i++)
    {
        cout << input[i] << ' ';
    }
    cout << endl;

    return 0;
}