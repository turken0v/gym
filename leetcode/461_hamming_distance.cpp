#include <iostream>
using namespace std;


int hamming_distance(int x, int y)
{
    bool a, b;
    int count = 0;
    while (x > 0 || y > 0)
    {
        a = x % 2 == 1;
        b = y % 2 == 1;

        count += a ^ b;

        x >>= 1;
        y >>= 1;
    }

    return count;
}


int main()
{
    cout << hamming_distance(1, 4) << endl;
    cout << hamming_distance(10, 17) << endl;

    return 0;
}