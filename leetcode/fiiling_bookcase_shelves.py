from typing import List, Tuple


def min_height_shelves(books: List[Tuple], shelf_width: int) -> int:
    n = len(books)
    shelf = []
    for i, current in enumerate(books):
        w, h = current

        if len(shelf) > 0 and n > i+1:
            pw, ph = shelf[-1]
            nw, _ = books[i+1]
            if h > ph and shelf_width - pw <= w + nw <= shelf_width:
                pass

        shelf.append(current)


if __name__ == '__main__':
    books = [
        (1,1),
        (2,3),
        (2,3),
        (1,1),
        (1,1),
        (1,1),
        (1,2),
    ]
    min_height_shelves(books, 4)