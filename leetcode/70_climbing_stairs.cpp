#include <iostream>
using namespace std;


int climb_stairs(int n)
{
    if (n < 1)
    {
        return 0;
    }

    int dp[n+2];
    dp[0] = 1;
    dp[1] = 1;
    dp[2] = 2;
    for (int i = 3; i < n+1; i++)
    {
        dp[i] = dp[i-2] + dp[i-1];
    }

    return dp[n];
}


int main()
{
    cout << climb_stairs(0) << endl;
    cout << climb_stairs(1) << endl;
    cout << climb_stairs(2) << endl;
    cout << climb_stairs(3) << endl;
    cout << climb_stairs(4) << endl;
    cout << climb_stairs(5) << endl;

    return 0;
}