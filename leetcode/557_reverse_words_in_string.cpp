#include <iostream>
#include <string>
#include <vector>
using namespace std;


string reverse_string(string input)
{
    int l = input.length(), n = l / 2;
    char t;
    for (int i = 0; i < n; i++)
    {
        t = input[l-1-i];
        input[l-1-i] = input[i];
        input[i] = t;
    }

    return input;
}

string reverse_words(string input)
{
    string w, output;
    int len = input.length(), j = 0;
    for (int i = 0; i < len; i++)
    {
        if (input[i] == ' ')
        {
            w = input.substr(j, i-j);
            output += reverse_string(w) + ' ';
            j = i+1;
        }
    }

    w = input.substr(j, len);
    output += reverse_string(w);

    return output;
}


int main()
{
    cout << "Hello, world!" << endl;
    cout << reverse_words("Hello, world!") << endl;
    return 0;
}