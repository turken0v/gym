#include <iostream>
#include <string>
using namespace std;


bool is_subsequence(string s, string t)
{
    if (s.length() == 0)
    {
        return true;
    }

    string::iterator ti = t.begin(), si = s.begin();
    while (ti != t.end())
    {
        if (*ti == *si)
        {
            si++;

            if (si == s.end())
            {
                return true;
            }
        }

        ti++;
    }

    return false;
}


int main()
{
    // cout << is_subsequence("abc", "ahbgdc") << endl;

    cout << is_subsequence("", "ahbgdc") << endl;

    return 0;
}