#include <iostream>
#include <vector>
using namespace std;


// int sum_combinations(vector<int>& nums, int n, int x, string i = "")
// {
//     if (x == 0)
//     {
//         return 1;
//     }

//     if (n <= 0 || x < 0)
//     {
//         cout << " -_-" << endl;
//         return 0;
//     }

//     string temp = i + " + " + to_string(nums[n-1]);

//     return sum_combinations(nums, n-1, x-nums[n-1], temp)
//         + sum_combinations(nums, n, x-nums[n-1], temp)
//         + sum_combinations(nums, n-1, x, i);
// }


// int num_sum_combinations(vector<int>& nums, int x)
// {
//     int n = nums.size();
//     return sum_combinations(nums, n, x);
// }



int64_t combination_sum(vector<int>& nums, int64_t x)
{
    int64_t dp[x+1];
    dp[0] = 1;

    int64_t n = nums.size();
    for (int64_t i = 1; i < x+1; i++)
    {
        dp[i] = 0;
        for (int64_t j = 0; j < n; j++)
        {
            if (i - nums[j] >= 0)
            {
                dp[i] += dp[i-nums[j]];
            }
        }
    }

    return dp[x];
}


int main()
{
    vector<int> nums = {3,33,333};
    cout << combination_sum(nums, 10000) << endl;
    return 0;
}