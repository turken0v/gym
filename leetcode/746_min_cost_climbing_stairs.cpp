#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;


int min_cost_climbing_stairs(vector<int>& cost)
{
    int n = cost.size();
    vector<int> dp = cost;
    for (int i = 2; i < n; i++)
    {
        dp[i] = min(dp[i-2], dp[i-1]) + cost[i];
    }

    return min(dp[n-2], dp[n-1]);
}


int main()
{
    vector<int> input {1, 100, 1, 1, 1, 100, 1, 1, 100, 1};
    cout << min_cost_climbing_stairs(input) << endl;

    return 0;
}