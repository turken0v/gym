#include <iostream>
#include <string>
using namespace std;


int cyclic(int index, int size)
{
    if (index >= size)
    {
        return index % size;
    }

    return index;
}


bool string_is_rotated(string a, string b)
{
    int n = a.length(), m = b.length();
    if (n + m == 0)
    {
        return true;
    }

    for (int i = 0; i < n; i++)
    {
        int k = i, j = 0;
        while (j < m && a[k] == b[j])
        {
            j++;
            k++;
            k = cyclic(k, n);
        }

        if (j == n)
        {
            return true;
        }
    }

    return false;
}


int main()
{
    cout << string_is_rotated("abcde", "cdeab");
    cout << string_is_rotated("abcde", "abced");

    return 0;
}