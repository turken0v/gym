#include <iostream>
#include <vector>
using namespace std;


// int max_profit(vector<int>& prices)
// {
//     int n = prices.size(), value, answer = -2147483647;
//     for (int i = 0; i < n; i++)
//     {
//         for (int j = i+1; j < n; j++)
//         {
//             value = prices[j] - prices[i];
//             answer = max(answer, value);
//         }
//     }

//     if (answer < 0)
//     {
//         answer = 0;
//     }

//     return answer;
// }


int max_profit(vector<int>& prices)
{
    vector<int> dp = prices;
    int n = prices.size(),
        answer = 0;
    for (int i = 1; i < n; i++)
    {
        dp[i] = min(dp[i], dp[i-1]);
        answer = max(answer, prices[i]-dp[i]);
    }

    return answer;
}


int main()
{
    vector<int> input {7,1,5,3,6,4};
    cout << max_profit(input) << endl;

    input = {7,6,4,3,1};
    cout << max_profit(input) << endl;

    input = {1};
    cout << max_profit(input) << endl;

    return 0;
}