#include <iostream>
#include <vector>
using namespace std;


bool intersection(int a1, int a2, int b1, int b2)
{
    return (b1 >= a1 && b1 < a2) || (b2 > a1 && b2 <= a2);
}


bool is_rectangle_overlap(vector<int>& first, vector<int>& second)
{
    int x1a = first[0],
        x1b = second[0],
        y1a = first[1],
        y1b = second[1],
        x2a = first[2],
        x2b = second[2],
        y2a = first[3],
        y2b = second[3];

    if ((x2a - x1a > 0) && (x2b - x1b > 0) && (y2a - y1a > 0) && (y2b - y1b > 0))
    {
        return (intersection(x1a, x2a, x1b, x2b) || intersection(x1b, x2b, x1a, x2a)) && (intersection(y1a, y2a, y1b, y2b) || intersection(y1b, y2b, y1a, y2a));
    }

    return false;
}


int main()
{
    vector<int> first, second;
    
    first = {0,0,2,2};
    second = {1,1,3,3};

    cout << is_rectangle_overlap(first, second) << endl;

    first = {0,0,1,1};
    second = {1,0,2,1};
    
    cout << is_rectangle_overlap(first, second) << endl;

    first = {0,0,1,1};
    second = {2,2,3,3};
    
    cout << is_rectangle_overlap(first, second) << endl;

    first = {7,8,13,15};
    second = {10,8,12,20};
    
    cout << is_rectangle_overlap(first, second) << endl;

    first = {4,4,14,7};
    second = {4,3,8,8};
    
    cout << is_rectangle_overlap(first, second) << endl;

    first = {-7,-3,10,5};
    second = {-6,-5,5,10};
    
    cout << is_rectangle_overlap(first, second) << endl;

    first = {-1,0,1,1};
    second = {0,-1,0,1};
    
    cout << is_rectangle_overlap(first, second) << endl;

    return 0;
}