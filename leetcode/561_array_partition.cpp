#include <iostream>
#include <bits/stdc++.h>
using namespace std;


int arrayPairSum(vector<int>& nums)
{
    vector<int> sorted = nums;
    int n = nums.size() / 2;
    sort(sorted.begin(), sorted.end());
    int answer = 0;
    for (int i = 0; i < n; i++)
    {
        sorted.pop_back();
        answer += sorted.back();
        sorted.pop_back();
    }
    
    return answer;
}

int main()
{
    
    return 0;
}