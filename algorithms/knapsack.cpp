#include<bits/stdc++.h>
using namespace std;


int knapsack(vector<int> V, vector<int> W, int w, int i)
{
    if (i < 0)
    {
        return 0;
    }

    if (W[i] > w)
    {
        return 0;
    }

    return max(V[i] + knapsack(V, W, w-W[i], i-1), knapsack(V, W, w, i-1));
}


int max_value_with_capacity(vector<int> V, vector<int> W, int w)
{
    int n = V.size();

    return knapsack(V, W, w, n-1);
}


int main()
{
    vector<int> values, weights;

    values = {60, 100, 120};
    weights = {10, 20, 30};
    cout << max_value_with_capacity(values, weights, 50) << endl;

    return 0;
}