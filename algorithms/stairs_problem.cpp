#include <bits/stdc++.h>
using namespace std;


int count_ways(int n) {
    int T[n+1];
    T[0] = 1;
    T[1] = 1;
    for (int i = 2; i < n+1; i++) {
        T[i] = T[i-1] + T[i-2];
    }

    return T[n];
}


int main() {

    cout << count_ways(3) << endl;
    cout << count_ways(4) << endl;
    cout << count_ways(5) << endl;
    cout << count_ways(6) << endl;

    return 0;
}