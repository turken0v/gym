from heapq import _heapify_max as heapify, _heappop_max as hpop


def knapsack(W, V, c):
    n = len(W)
    if n != len(V):
        raise ValueError

    R = [(V[i]/W[i], (V[i], W[i])) for i in range(n)]
    heapify(R)

    A = []
    tw = 0
    for _ in range(n):
        _, k = hpop(R)
        v, w = k
        tw += w
        if c > tw:
            A.append(k)

    return A


if __name__ == '__main__':
    W = [10, 20, 30]
    V = [60, 100, 120]
    c = 50
    print(knapsack(W, V, c))