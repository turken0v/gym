from data_structures.stack import Stack


def is_operand(char: str) -> bool:
    return char.isalpha()


PRECEDENCE = {
    '+': 1,
    '-': 1,

    '*': 2,
    '/': 2,

    '^': 3
}

def precedence_gte(a: str, b: str):
    pa = PRECEDENCE.get(a, -1)
    pb = PRECEDENCE.get(b, -1)
    return pa >= pb


def infix2postfix(I: str) -> str:
    """
    Returns infix expression to postfix
    f('a+b*c') = 'abc*+'
    """
    P = ''
    O = Stack()
    for c in I:
        if is_operand(c):
            P += c
        elif c == '(':
            O.append(c)
        elif c == ')':
            while not O.is_empty() and O.top != '(':
                P += O.pop()

            if not O.is_empty():
                O.pop()
        else:
            while not O.is_empty() and precedence_gte(O.top, c):
                P += O.pop()

            O.append(c)

    P += ''.join(O)

    return P


def evaluation_postfix(E: str):
    O = Stack()
    for e in E:
        if e.isdigit():
            O.append(e)
        else:
            v1 = O.pop()
            v2 = O.pop()

            ans = str(eval(v2 + e + v1))
            O.append(ans)
    
    return int(O.pop())

if __name__ == '__main__':
    print(evaluation_postfix('231*+9-'))