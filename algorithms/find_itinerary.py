from reverse_dictionary import reverse_dictionary
from typing import List, Dict


def argmax(elements: List) -> int:
    max_index = 0
    for i, value in enumerate(elements, start=1):
        max_value = elements[max_index]
        if value > max_value:
            max_index = i

    return max_index


# O(n^2)
def find_itinerary(routes: Dict[str, str]) -> List[str]:
    answer = []
    cities = set(list(routes.keys()) + list(routes.values()))
    for city in cities:
        route = [city]
        next_city = routes.get(city, None)
        while next_city:
            route.append(next_city)
            next_city = routes.get(next_city, None)

        answer.append(route)

    largest_route_index = argmax([len(route) for route in answer])

    return answer[largest_route_index]


# O(n)
def find_itinerary(routes) -> List:
    reversed_routes = reverse_dictionary(routes)
    starting_point = None
    for key in routes.keys():
        found = reversed_routes.get(key, None)
        if found is None:
            starting_point = key

    if starting_point is None:
        return False

    route = [starting_point]
    next_city = routes.get(starting_point, None)
    while next_city:
        route.append(next_city)
        next_city = routes.get(next_city, None)

    return route


if __name__ == '__main__':
    routes = {
        'Chennai': 'Banglore',
        'Bombay': 'Delhi',
        'Goa': 'Chennai',
        'Delhi': 'Goa'
    }
    print(find_itinerary(routes))