#include <bits/stdc++.h>
using namespace std;


int min_cost_path(vector<vector<int>> &matrix)
{
    int n = matrix.size();

    int T[n][n];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            T[i][j] = matrix[i][j];
        }
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            int v = INT_MAX;
            bool found = false;

            if (i > 0)
            {
                v = min(v, T[i-1][j]);
                found = true;
            }

            if (j > 0)
            {
                v = min(v, T[i][j-1]);
                found = true;
            }

            if (i > 0 && j > 0)
            {
                v = min(v, T[i-1][j-1]);
                found = true;
            }

            if (found)
            {
                T[i][j] += v;
            }
        }
    }

    return T[n-1][n-1];
}


int main()
{
    vector<vector<int>> matrix {
        {1, 8, 8, 1, 5},
        {4, 1, 1, 8, 1},
        {4, 2, 8, 8, 1},
        {1, 5, 8, 8, 1}
    }; 

    cout << min_cost_path(matrix) << endl;

    return 0;
}