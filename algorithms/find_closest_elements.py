from heapq import heapify, heappush as hpush, heappop as hpop
from typing import List


# O(nlogn)
def find_closest_elements(elements: List, x, k: int) -> List:
    answer = []

    left = []
    right = []
    for value in elements: # O(n)
        if value > x:
            hpush(right, value) # O(logn)
        elif x > value:
            hpush(left, -value) # O(logn)
        else:
            answer.append(value)
    # O(nlogn)

    try: answer.pop()
    except: pass

    # m = len(answer)
    k -= len(answer)

    i = 0
    while k > i: # O(k - m)
        lclosest = -left[0]
        rclosest = right[0]

        if rclosest - x > x - lclosest:
            hpop(left) # O(logk)
            answer.append(lclosest)
        else:
            hpop(right) # O(logk)
            answer.append(rclosest)

        i += 1
    # O((k-m)logk)

    return answer



if __name__ == '__main__':
    a = [12, 16, 22, 30, 35, 35, 39, 42, 45, 48, 50, 53, 55, 56]
    print(find_closest_elements(a, 35, 4))