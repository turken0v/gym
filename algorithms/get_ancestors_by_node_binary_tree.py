from data_structures.binary_tree import Node
from data_structures.stack import Stack
from typing import List, Iterable

# # O(n)
# # S(n)
# def get_ancestors_by_node(root: Node, required: Node) -> List:
#     first_element = root, 0
#     buffer = Stack([first_element])
#     explored = []
#     hope = True
#     level = 0
#     while buffer:
#         i = level - buffer.top[1]
#         while not hope and explored and i > 0:
#             explored.pop()
#             i -= 1

#         hope = False
#         node, level = buffer.pop()
#         explored.append(node)
#         if node == required:
#             return explored

#         if isinstance(node.right, Node):
#             element = node.right, level+1
#             buffer.append(element)
#             hope = True

#         if isinstance(node.left, Node):
#             element = node.left, level+1
#             buffer.append(element)
#             hope = True

# O(n)
# S(1)
def _ancestors_by_node(root: Node, required: Node, answer: List) -> bool:
    if root == required:
        answer.append(root)
        return True

    if isinstance(root.left, Node):
        found = _ancestors_by_node(root.left, required, answer)
        if found:
            answer.append(root)
            return True

    if isinstance(root.right, Node):
        found = _ancestors_by_node(root.right, required, answer)
        if found:
            answer.append(root)
            return True


def get_ancestors_by_node(root: Node, required: Node) -> Iterable[Node]:
    answer = []
    _ancestors_by_node(root, required, answer)
    return reversed(answer)


if __name__ == '__main__':
    _1 = Node(1)
    _2 = Node(2)
    _3 = Node(3)
    _4 = Node(4)
    _5 = Node(5)
    _7 = Node(7)

    _1.left = _2
    _1.right = _3
    _2.left = _4
    _2.right = _5
    _4.left = _7

    print(get_ancestors_by_node(_1, _5))