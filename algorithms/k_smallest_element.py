from heapq import heapify, heappop as hpop
from typing import List


# def kth_smallest(elements: List, k: int) -> List:
#     assert len(elements) >= k, ValueError

#     heapify(elements)
#     required = None
#     for _ in range(k):
#         required = hpop(elements)
    
#     return required

def partition(elements: List, l, r):
    x = elements[r]
    i = l
    for j in range(l, r):
        if x >= elements[j]:
            elements[i], elements[j] = elements[j], elements[i]
            i += 1

    elements[i], elements[r] = elements[r], elements[i]

    return i


def kth_smallest(elements: List, k: int, l: int = None, r: int = None) -> List:
    if l is None and r is None:
        l, r = 0, len(elements)-1

    if r-l+1 >= k:
        p = partition(elements, l, r)

        if p-l == k-1:
            return elements[p]

        if p-l > k-1:
            return kth_smallest(elements, k, l, p-1)
        
        return kth_smallest(elements, k-p+l-1, p+1, r)


if __name__ == '__main__':
    a = [7, 10, 4, 3, 20, 15]
    print(kth_smallest(a, 4))