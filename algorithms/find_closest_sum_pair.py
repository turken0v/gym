from algorithms.arg_funcs import arg_min
from typing import List, Tuple


def find_closest_sum_pair(elements: List, x: int) -> Tuple[any, any]:
    n = len(elements)
    l, r = 0, n-1
    pairs = []
    D = []
    while r > l:
        delta = elements[l] + elements[r] - x
        _ = elements[l], elements[r]
        pairs.append(_)
        _ = abs(delta)
        D.append(_)

        if delta > 0:
            r -= 1
        else:
            l += 1

    i, _ = arg_min(D)

    return pairs[i]


if __name__ == '__main__':
    a = [10, 22, 28, 29, 30, 40]
    print(find_closest_sum_pair(a, 54))
    a = [1, 3, 4, 7, 10]
    print(find_closest_sum_pair(a, 15))