#include <bits/stdc++.h>
using namespace std;


int factorial(int n)
{
    int answer = 1;
    for (int i = 2; i <= n; i++)
    {
        answer *= i;
    }

    return answer;
}

int C(int n, int k)
{
    return factorial(n) / (factorial(k) * factorial(n-k));
}



int main()
{
    cout << C(10, 2) << endl;

    return 0;
}