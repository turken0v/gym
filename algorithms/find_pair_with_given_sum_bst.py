from data_structures.binary_tree import Node, print_inorder_binary_tree
from typing import Dict


def has_pair(root: Node, required, nodes: Dict[int, bool] = {}) -> bool:
    if isinstance(root.left, Node):
        if has_pair(root.left, required, nodes):
            return True
 
    if nodes.get(required - root.data):
        return True

    nodes[root.data] = True

    if isinstance(root.right, Node):
        if has_pair(root.right, required, nodes):
            return True

    return False


if __name__ == '__main__':
    _15 = Node(15)
    _20 = Node(20)
    _16 = Node(16)
    _25 = Node(25)
    _10 = Node(10)
    _12 = Node(12)
    _8 = Node(8)

    _15.left = _10
    _15.right = _20
    _10.left = _8
    _10.right = _12
    _20.left = _16
    _20.right = _25

    # print_inorder_binary_tree(_15)
    print(has_pair(_15, 29))