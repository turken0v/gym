from data_structures.binary_tree import Node
from depth_first_traversal import inorder_traversal
from three_sum import three_sum


def is_triplet(root: Node):
    nodes = inorder_traversal(root)
    
    return three_sum(nodes)

if __name__ == '__main__':
    __13 = Node(-13)
    __8 = Node(-8)
    _6 = Node(6)
    _7 = Node(7)
    _13 = Node(13)
    _14 = Node(14)
    _15 = Node(15)

    _6.left = __13
    _6.right = _14
    __13.right = __8
    _14.left = _13
    _14.right = _15
    _13.left = _7

    print(is_triplet(_6))