from typing import List
from functools import reduce
import operator

# def prod(A):
#     return reduce(operator.mul, A, 1)

# def product(A: List) -> List:
#     count = len(A)
#     M = [0 for _ in range(count)]
#     S = [len(it) for it in A]
#     total_count = prod(S)

#     P = []
#     counter = 0
#     while total_count > counter:
#         for i in range(1, count+1):
#             i = -i

#             if M[i] >= S[i]:
#                 M[i-1] += 1
#                 M[i] = 0
#             else:
#                 break

#         T = []
#         for r in range(count):
#             idx = M[r]
#             T.append(A[r][idx])

#         M[-1] += 1
#         P.append(T)
#         counter += 1

#     return P


def product(args):
    pools = [tuple(pool) for pool in args]
    result = [[]]
    for pool in pools:
        result = [x+[y] for x in result for y in pool]

    return result


if __name__ == "__main__":
    a = product(
        [1,2,3],
        [4,5,6],
        [7,8,9],    
    )
    print(a)