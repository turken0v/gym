from data_structures.stack import Stack
from typing import List

# def stock_span(prices: List[int]) -> List[int]:
#     answer = []
#     for i, price in enumerate(prices):
#         span = 1

#         S = Stack(prices[:i])
#         while not S.is_empty() and price > S.top:
#             span += 1
#             S.pop()

#         answer.append(span)

#     return answer


def stock_span(prices: List[int]) -> List[int]:
    answer = []
    size = len(prices)
    S = Stack()
    _ = prices[0], 1,
    S.append(_)
    for i in range(1, size):
        price = prices[i]
        span = 1

        while not S.is_empty() and price > S.top[0]:
            _, last_span = S.pop()
            span += last_span

        _ = price, span,
        S.append(_)
        answer.append(span)

    return answer

if __name__ == '__main__':
    print(stock_span([100,80,60,70,60,75,85]))