#include <bits/stdc++.h>
using namespace std;


// string hash_pair(int a, int b) {
//     return to_string(a) + '$' + to_string(b);
// }


// int L(string &input, int l, int r, unordered_map<string, int> &M) {
//     if (l > r) {
//         return 0;
//     }

//     if (l == r) {
//         return 1;
//     }

//     string key = hash_pair(l, r);
//     if (M[key] > 0) {
//         return M[key];
//     }

//     int length = 0;
//     for (int i = l+1; i <= r; i++) {
//         if (input[i] == input[l]) {
//             length = max(length, L(input, l+1, i-1, M)+2);
//         }
//     }

//     M[key] = max(length, L(input, l+1, r, M));

//     return M[key];
// }

// int LPS(string input) {
//     unordered_map<string, int> M;
//     return L(input, 0, input.length()-1, M);
// }


int LPS(string I) {
    int n = I.length();

    int T[n][n];
    for (int i = 0; i < n; i++) {
        T[i][i] = 1;
    }

    int l, i, j;
    for (l = 2; l <= n; l++) {
        for (i = 0; i < n-l+1; i++) {
            j = i+l-1;

            if (I[i] == I[j]) {
                if (l > 2) {
                    T[i][j] = T[i+1][j-1] + 2;
                } else {
                    T[i][j] = 2;
                }
            } else {
                T[i][j] = max(T[i+1][j], T[i][j-1]);
            }
        }
    }

    return T[0][n-1];
}


int main() {
    string input;

    input = "BBABCBCAB";
    cout << LPS(input) << endl;

    // input = "GEEKSFORGEEKS";
    // cout << LPS(input) << endl;

    return 0;
}