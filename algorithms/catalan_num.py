def catalan_num(n: int) -> int:
    if n <= 1:
        return 1

    answer = 0
    for i in range(n):
        answer += catalan_num(i) * catalan_num(n-i-1)

    return answer


if __name__ == '__main__':
    print(catalan_num(3))