from data_structures.deque import Deque
from data_structures.binary_tree import Node
from typing import List


def level_order_traversal(root: Node) -> List[any]:
    answer = []
    deq = Deque([root])
    while deq:
        node = deq.pop()

        if isinstance(node.left, Node):
            deq.appendleft(node.left)

        if isinstance(node.right, Node):
            deq.appendleft(node.right)

        answer.append(node.data)

    return answer


if __name__ == '__main__':
    left = Node(2, left=Node(4), right=Node(5))
    right = Node(3, left=Node(6))
    root = Node(1, left=left, right=right)
    print(level_order_traversal(root))