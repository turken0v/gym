def GCD(a: int, b: int) -> int:
    while a != 0 and b != 0:
        if a > b:
            a %= b
        else:
            b %= a

    return a + b


if __name__ == '__main__':
    print(GCD(30, 15))