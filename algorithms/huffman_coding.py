from heapq import heapify, heappop as hpop, heappush as hpush
from typing import Tuple, List


def huffman_coding(V: List[Tuple[int, str]]):
    A = V[:]
    B = []
    C = []
    heapify(A)

    def _min(A: List[Tuple[int, str]], B: List[Tuple[int, str]]):
        if len(B) == 0:
            return hpop(A)

        if len(A) == 0:
            return hpop(B)

        a = A[0][0]
        b = B[0][0]
        return hpop(A) if b > a else hpop(B)

    i = 1
    N = None
    while A:
        left = _min(A, B)
        right = _min(A, B)

        freq = left[0] + right[0]
        value = 'N' + str(i)
        N = freq, value
        hpush(B, N)
        i += 1

        hpush(C, left)
        hpush(C, right)

    hpush(C, N)
    return C


if __name__ == '__main__':
    V = [
        (5, 'a'),
        (9, 'b'),
        (12, 'c'),
        (13, 'd'),
        (16, 'e'),
        (45, 'f'),
    ]
    print(huffman_coding(V))