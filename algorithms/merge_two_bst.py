from data_structures.binary_tree import Node, print_inorder_binary_tree
from data_structures.stack import Stack
from typing import List
from construct_bst import construct_bst


def inorder_iterative(root):
    stack = Stack()
    node = root
    while stack or node:
        if node:
            stack.append(node)
            node = node.left
        else:
            node = stack.pop()
            yield node
            node = node.right


def merge_two_bst(aroot: Node, broot: Node) -> List[any]:
    a = inorder_iterative(aroot)
    b = inorder_iterative(broot)
    anode = next(a, None)
    bnode = next(b, None)
    answer = []
    while anode or bnode:
        if anode is None:
            answer.append(bnode)
            bnode = next(b, None)
            continue

        if bnode is None:
            answer.append(anode)
            anode = next(a, None)
            continue

        if anode > bnode:
            answer.append(bnode)
            bnode = next(b, None)
        else:
            answer.append(anode)
            anode = next(a, None)

    return answer


def merge2into1bst(aroot: Node, broot: Node) -> List[any]:
    array = merge_two_bst(aroot, broot)
    return construct_bst(array)


if __name__ == '__main__':
    # _20 = Node(0)
    # _11 = Node(1)
    # _12 = Node(2)
    # _23 = Node(3)
    # _25 = Node(5)
    # _18 = Node(8)
    # _110 = Node(10)

    # _18.left = _12
    # _18.right = _110
    # _12.left = _11

    # _25.left = _23
    # _23.left = _20

    # print(merge_two_bst(_18, _25))

    _7 = Node(7)
    _5 = Node(5)
    _8 = Node(8)
    _4 = Node(4)
    _6 = Node(6)
    _9 = Node(9)
    _1 = Node(1)
    _2 = Node(2)
    _3 = Node(3)

    _7.left = _5
    _7.right = _8
    _5.left = _4
    _5.right = _6
    _8.right = _9

    _2.left = _1
    _2.right = _3
    
    merged = merge2into1bst(_7, _2)
    print_inorder_binary_tree(merged)