from data_structures.binary_tree import Node
from data_structures.stack import Stack


# O(n)
def diameter_binary_tree(root: Node) -> int:
    element = [root]
    S = Stack([element])
    max_level = 0
    path2max = []
    while S:
        path = S.pop()
        level = len(path)
        if level > max_level:
            max_level = level
            path2max = path

        node = path[-1]
        if isinstance(node.right, Node):
            element = path + [node.right]
            S.append(element)

        if isinstance(node.left, Node):
            element = path + [node.left]
            S.append(element)

    element = root, 0
    S = Stack([element])
    max2_level = 0
    while S:
        node, level = S.pop()
        if level > max2_level:
            max2_level = level

        if isinstance(node.right, Node):
            element = node.right, 0 if node.right in path2max else level+1
            S.append(element)

        if isinstance(node.left, Node):
            element = node.left, 0 if node.left in path2max else level+1
            S.append(element)

    return max_level + max2_level


if __name__ == '__main__':
    left = Node(6)
    left = Node(5, left=left)
    left = Node(4, left=left)
    right = Node(10)
    right = Node(11, right=right)
    right = Node(12, right=right)
    left = Node(3, left=left, right=right)
    left = Node(2, left=left)
    right = Node(9)
    right = Node(8, right=right)
    right = Node(7, right=right)
    root = Node(1, left=left, right=right)
    print(diameter_binary_tree(root))