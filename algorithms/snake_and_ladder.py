from typing import List


def find_min_dice_throws(board: List[int]) -> int:
    size = len(board)
    pos = 0
    num = 0
    while size-6 > pos:
        steps = []
        for i in range(6):
            i += pos
            step = board[i]
            steps.append(i if step < 0 else step)

        pos = max(steps)
        num += 1

    return num+1


if __name__ == '__main__':
    board = [
        -1,  -1, 21, -1,  7, -1,
        -1,  -1,  3, -1, 25, -1,
        -1,  -1, -1, -1, -1, -1,
         6,  28,  8, -1, -1, -1,
        -1,  -1,  0, -1, -1, -1
    ]
    print(find_min_dice_throws(board))