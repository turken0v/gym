from typing import List, Tuple


def has_pair4sum(elements: List, target: any) -> Tuple[any, any]:
    buffer = {}
    for value in elements:
        required = target - value
        found = buffer.get(required, None)
        if found:
            return value, required

        buffer[value] = True

    return False


if __name__ == '__main__':
    print(has_pair4sum([0, -1, 2, -3, 1], -2))
    print(has_pair4sum([1, -2, 1, 0, 5], 0))