from data_structures.binary_tree import Node, print_inorder_binary_tree


def find_broken_nodes(root: Node, l = None, r = None, answer=[]):
    if l and l > root.data or r and root.data > r:
        answer.append(root)

    if isinstance(root.left, Node):
        find_broken_nodes(root.left, l, root.data)

    if isinstance(root.right, Node):
        find_broken_nodes(root.right, root.data, r)

    return answer


def swap_nodes(a: Node, b: Node):
    a.data, b.data = b.data, a.data


def fix2swapped(root: Node):
    nodes = find_broken_nodes(root)
    assert len(nodes) == 2, ''
    swap_nodes(*nodes)


if __name__ == '__main__':
    _10 = Node(10)
    _5 = Node(5)
    _8 = Node(8)
    _2 = Node(2)
    _20 = Node(20)

    _10.left = _5
    _10.right = _8
    _5.left = _2
    _5.right = _20

    fix2swapped(_10)