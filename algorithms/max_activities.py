def max_activities(S, F):
    count = len(S)
    if count != len(F):
        raise ValueError

    M = []
    for i in range(count):
        C = [(S[i], F[i])]
        f = F[i]
        for j in range(i+1, count):
            a = S[j]
            b = F[j]
            if a >= f:
                C.append((a, b))
                f = b

        M += [C]

    return max(M, key = lambda k: len(k))


if __name__ == '__main__':
    S = [1, 3, 0, 5, 8, 5]
    F = [2, 4, 6, 7, 9, 9]

    print(max_activities(S, F))