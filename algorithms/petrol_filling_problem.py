from heapq import _heapify_max as heapify, _heappop_max as hpop
from typing import List, Tuple

# def find_petrol_pump(pumps: List[Tuple[int, int]]) -> int:
#     size = len(pumps)
#     for i in range(size):
#         counter = 0
#         for pump in pumps:
#             distance, petrol = pump
#             counter += petrol - distance

#             if counter < 0:
#                 break

#         if counter >= 0:
#             return i

#         pump = pumps.pop(0)
#         pumps.append(pump)


def find_petrol_pump(pumps: List[Tuple[int, int]]) -> int:
    PQ = []
    for i, pump in enumerate(pumps):
        distance, petrol = pump
        PQ.append((petrol - distance, i))

    heapify(PQ)
    counter, i = hpop(PQ)
    while PQ and counter >= 0:
        k, _ = hpop(PQ)
        counter += k

    return i


if __name__ == '__main__':
    print(find_petrol_pump([
        (6,4),
        (3,6),
        (7,3),
    ]))