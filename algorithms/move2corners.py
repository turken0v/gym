# https://www.geeksforgeeks.org/minimum-adjacent-swaps-to-move-maximum-and-minimum-to-corners/
from algorithms.arg_funcs import arg_max, arg_min
from typing import List


def move2corner(elements: List) -> int:
    n = len(elements) - 1

    i1, _ = arg_max(elements)
    counter = i1

    i2, _ = arg_min(elements)
    if i1 > i2:
        counter -= 1
    counter += n - i2

    return counter


if __name__ == '__main__':
    a = [3, 1, 5, 3, 5, 5, 2]
    print(move2corner(a))
    a = [5, 6, 1, 3]
    print(move2corner(a))