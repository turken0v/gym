#include <bits/stdc++.h>
using namespace std;


int max_sum_increasing_subsequence(vector<int> &nums)
{
    int n = nums.size();

    int T[n];
    memset(T, 0, sizeof(T));

    int answer = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = i-1; j > -1; j--)
        {
            if (nums[i] >= nums[j])
            {
                T[i] = max(T[i], T[j]);
            }
        }

        T[i] += nums[i];

        answer = max(answer, T[i]);
    }

    return answer;
}


int main()
{
    vector<int> input;

    input = {1,101,2,3,100,4,5};
    cout << max_sum_increasing_subsequence(input) << endl;

    return 0;
}