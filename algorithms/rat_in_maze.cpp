#include <iostream>
#include <vector>
#include <map>
using namespace std;


int distance(int xa, int ya, int xb, int yb)
{
    return abs(xa - xb) + abs(ya - yb);
}


bool traverse_by_maze(
    vector<vector<bool>> &maze,
    vector<vector<bool>> &path,
    int m, int n,
    int X, int Y, int x = 0, int y = 0
)
{
    if (x == X && y == Y)
    {
        path[y][x] = true;
        return true;
    }

    multimap<int, pair<int, int>> directions;
    int key, a, b;
    for (int i = -1; i < 2; i++)
    {
        for (int j = -1; j < 2; j++)
        {
            a = x+j;
            b = y+i;

            if (a < 0 || a >= m || b < 0 || b >= n)
            {
                continue;
            }

            if (maze[b][a] == true)
            {
                key = distance(x+j, y+i, X, Y);
                directions.insert(make_pair(key, make_pair(x+j, y+i)));
            }
        }
    }

    for (
        auto it = directions.begin();
        it != directions.end();
        ++it
    )
    {
        a = it->second.first;
        b = it->second.second;

        if (traverse_by_maze(maze, path, n, m, X, Y, a, b))
        {
            path[y][x] = true;
            return true;
        }
    }

    return false;
}


vector<vector<bool>> path2destination(
    vector<vector<bool>> &maze,
    int X, int Y, int x = 0, int y = 0
)
{
    int n = maze.size(),
        m = maze[0].size();

    vector<vector<bool>> path (n, vector<bool> (m));

    traverse_by_maze(maze, path, n, m, X, Y, x, y);

    return path;
}


int main()
{
    vector<vector<bool>> maze {
        {true, false, false, false},
        {true, true, false, true},
        {false, true, false, false},
        {true, true, true, true}
    };

    vector<vector<bool>> path = path2destination(maze, 3, 3);

    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            cout << path[i][j] << ' ';
        }

        cout << endl;
    }

    return 0;
}