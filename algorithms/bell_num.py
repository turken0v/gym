# https://www.geeksforgeeks.org/bell-numbers-number-of-ways-to-partition-a-set/


from stirling_num import count_parts


# # Stirling numbers method
# def bell(n: int) -> int:
#     s = 0
#     for i in range(n):
#         s += count_parts(n, i+1)

#     return s


# Bell Triangle method
# 1 
# 1  2
# 2  3  5
# 5  7  10 15
# 15 20 27 32 52
def bell(n: int) -> int:
	bell = [[0 for i in range(n+1)] for j in range(n+1)]
	bell[0][0] = 1
	for i in range(1, n+1):
		bell[i][0] = bell[i-1][i-1]

		for j in range(1, i+1):
			bell[i][j] = bell[i-1][j-1] + bell[i][j-1]

	return bell[n][0]


if __name__ == '__main__':
    print(bell(500))