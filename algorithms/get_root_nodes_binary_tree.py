from data_structures.binary_tree import Node
from typing import List

# O(n)
def _nodes_by_level(root: Node, level: int, answer: List, i = 0):
    if i == level:
        answer.append(root)

    if isinstance(root.left, Node):
        _nodes_by_level(root.left, level, answer, i+1)

    if isinstance(root.right, Node):
        _nodes_by_level(root.right, level, answer, i+1)


def get_nodes_by_level(root: Node, level:int = 1) -> List:
    if root is None:
        return None

    answer = []
    _nodes_by_level(root, level, answer)
    return answer


if __name__ == '__main__':
    _1 = Node(1)
    _2 = Node(2)
    _3 = Node(3)
    _4 = Node(4)
    _5 = Node(5)
    _6 = Node(6)
    _7 = Node(7)
    _8 = Node(8)

    _1.left = _2
    _1.right = _3
    _2.left = _4
    _2.right = _5
    _3.right = _8
    _8.left = _6
    _8.right = _7

    print(get_nodes_by_level(_1, 2))