#include <bits/stdc++.h>
using namespace std;


int longest_bitonic_subsequence(vector<int> &nums)
{
    int n = nums.size();

    int F[n], B[n];
    memset(F, 0, sizeof(F));
    memset(B, 0, sizeof(B));

    for (int i = 0; i < n; i++)
    {
        for (int j = i-1; j > -1; j--)
        {
            if (nums[i] >= nums[j])
            {
                F[i] = max(F[i], F[j]+1);
            }
        }
    }

    for (int i = n-1; i > -1; i--)
    {
        for (int j = i+1; j < n; j++)
        {
            if (nums[i] >= nums[j])
            {
                B[i] = max(B[i], B[j]+1);
            }
        }
    }

    int length = 0;
    for (int i = 0; i < n; i++)
    {
        length = max(length, F[i] + B[i] + 1);
    }

    return length;
}


int main()
{
    vector<int> input;

    input = {1, 11, 2, 10, 4, 5, 2, 1};
    cout << longest_bitonic_subsequence(input) << endl;

    input = {12, 11, 40, 5, 3, 1};
    cout << longest_bitonic_subsequence(input) << endl;

    input = {80, 60, 30, 40, 20, 10};
    cout << longest_bitonic_subsequence(input) << endl;

    return 0;
}