def reverse_string(S: str) -> str:
    I = list(S)
    R = [I.pop() for _ in range(len(I))]
    return ''.join(R)


if __name__ == '__main__':
    print(reverse_string('hello'))