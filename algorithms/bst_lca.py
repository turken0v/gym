# Lowest Common Ancestor
from data_structures.binary_tree import Node

# log(n)
def lowest_common_ancestor(root: Node, l: int, r: int) -> Node:
    if r >= root.data >= l:
        return root
    elif root.data >= r >= l:
        if isinstance(root.left, Node):
            return lowest_common_ancestor(root.left, l, r)
    elif r >= l >= root.data:
        if isinstance(root.right, Node):
            return lowest_common_ancestor(root.right, l, r)


if __name__ == '__main__':
    _20 = Node(20)
    _8 = Node(8)
    _22 = Node(22)
    _4 = Node(4)
    _12 = Node(12)
    _10 = Node(10)
    _14 = Node(14)

    _20.left = _8
    _20.right = _22
    _8.left = _4
    _8.right = _12
    _12.left = _10
    _12.right = _14

    print(lowest_common_ancestor(_20, 10, 22))
