from factorial import factorial

# # Factorial solution
# def C(n, k):
#     if k > n:
#         raise ValueError

#     if k == 0:
#         return 1

#     if k == 1:
#         return n

#     if n == k:
#         return 1

#     return factorial(n) / (factorial(n-k) * factorial(k))


# # Recursion solution
# def C(n, k):
#     if k > n:
#         raise ValueError

#     if k == 0:
#         return 1

#     if k == 1:
#         return n

#     if n == k:
#         return 1

#     return C(n-1, k-1) + C(n-1, k)


# Pascal Triangle solution
def C(n, k):
    if k > n:
        raise ValueError

    if k == 1:
        return n

    if k == 0 or n == k:
        return 1

    T = [1]
    for i in range(1, n+1):
        for j in range(i+1):
            if j == 0 or j == i:
                T.append(1)
                continue

            t = i - 1
            a = int(0.5 * (t**2 + t) + j)
            c = T[a - 1] + T[a]
            T.append(c)

    i = int(0.5 * (n**2 + n) + k)
    return T[i]


if __name__ == '__main__':
    print(C(6, 2))