def arg_max(elements):
    index = 0
    largest = elements[0]
    for i, value in enumerate(elements):
        if value > largest:
            index = i
            largest = value

    return index, largest


def arg_min(elements):
    index = 0
    smallest = elements[0]
    for i, value in enumerate(elements, 0):
        if smallest > value:
            index = i
            smallest = value

    return index, smallest


if __name__ == '__main__':
    a = [40, 30, 20, 10]
    print(arg_min(a))