from typing import List


def is_subset(a: List, b: List) -> bool:
    buffer = {}
    for value in a:
        buffer[value] = True

    for value in b:
        found = buffer.get(value, None)
        if found is None:
            return False

    return True


def intersection(a: List, b: List) -> List:
    buffer = {}
    for value in a:
        buffer[value] = True

    answer = []
    for value in b:
        found = buffer.get(value, None)
        if found:
            answer.append(value)

    return answer


def union(a: List, b: List) -> List:
    buffer = {}
    for value in a:
        buffer[value] = True

    for value in b:
        buffer[value] = True

    return buffer.keys()


if __name__ == '__main__':
    a = [1, 2, 3, 4, 5, 6]
    b = [1, 2, 4]
    print(is_subset(a, b))

    a = [10, 15, 4, 20]
    b = [8, 4, 2, 10]

    print(intersection(a, b))
    print(union(a, b))