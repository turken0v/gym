#include <bits/stdc++.h>
using namespace std;


int sum(vector<int> &nums)
{
    int counter = 0;
    for (int i = 0; i < nums.size(); i++)
    {
        counter += nums[i];
    }

    return counter;
}


bool has_two_subsets_equal_sum(vector<int> &nums)
{
    int n = nums.size(),
        l = sum(nums) + 1;

    bool A[l];
    memset(A, false, sizeof(bool)*l);
    A[0] = true;

    bool B[l][n];
    memset(B, false, sizeof(bool)*l*n);

    vector<int> C;
    for (int i = 1; i < l; i++)
    {
        C = {};
        for (int j = n-1; j > -1; j--)
        {
            if (nums[j] > i || A[i-nums[j]] == false || B[i-nums[j]][j] == true)
            {
                continue;
            }

            bool state;
            int v = 0;
            for (int k = 0; k < n; k++)
            {
                state = k == j ? true : B[i-nums[j]][k];

                B[i][k] = state;
            }

            A[i] = true;
        }

        if (C.size() > 1 && )
        {
            return true;
        }
    }

    return false;
}

int main()
{
    vector<int> input;

    input = {1,5,11,5};
    cout << has_two_subsets_equal_sum(input) << endl;

    input = {1, 2, 4};
    cout << has_two_subsets_equal_sum(input) << endl;

    input = {1, 2, 3, 4};
    cout << has_two_subsets_equal_sum(input) << endl;

    return 0;
}