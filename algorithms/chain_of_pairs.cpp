#include <bits/stdc++.h>
using namespace std;


int max_length_pairs_chain(vector<pair<int, int>> &chain)
{
    int n = chain.size();

    int T[n];
    memset(T, 0, sizeof(int)*n);

    int length = 0;
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (chain[i].first >= chain[j].second)
            {
                T[i] = max(T[i], T[j]+1);
                length = max(length, T[i]+1);
            }
        }
    }

    return length;
}


int main()
{
    vector<pair<int, int>> input;

    input = {{5, 24}, {39, 60}, {15, 28}, {27, 40}, {50, 90}};
    cout << max_length_pairs_chain(input) << endl;

    return 0;
}