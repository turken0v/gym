from typing import List


def reverse_in_groups(A: List, k: int = 1):
    s = len(A)
    m = k // 2
    i, j, l, r = 0, 0, 0, k
    while s > r:
        A[l], A[r] = A[r], A[l]

        j += 1

        if j >= m:
            i += 1
            j = 0

        l = i * k + j + i
        r = (i + 1) * k - j + i


if __name__ == '__main__':
    A = [0,1,2,3,4,5,6,7,8,9]
    print(A)
    reverse_in_groups(A, 2)
    print(A)