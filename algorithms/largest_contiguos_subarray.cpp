#include <bits/stdc++.h>
using namespace std;


int LCS(vector<int> &nums) {
    int n = nums.size();

    int T[n];
    T[0] = nums[0] > 0 ? nums[0] : 0;

    int answer = 0;
    for (int i = 1; i < nums.size(); i++) {
        if (nums[i]+T[i-1] > 0) {
            T[i] = nums[i] + T[i-1];
            answer = max(answer, T[i]);
        } else {
            T[i] = 0;
        }
    }

    return answer;
}


int main() {
    vector<int> input;

    input = {-2,-3,4,-1,-2,1,5,-3};
    cout << LCS(input) << endl;

    return 0;
}