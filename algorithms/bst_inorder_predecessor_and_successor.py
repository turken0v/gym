from data_structures.binary_tree import Node
from typing import Tuple
from depth_first_traversal import inorder_traversal
from math import floor
from depth_first_traversal import left_most, right_most


# def inorder_predecessor_and_successor(root: Node, required: any) -> Tuple[Node, Node]:
#     nodes = inorder_traversal(root)
#     size = len(nodes)
#     a, b = 0, size-1
#     while floor((b - a) / 2) > 0:
#         mid = round((b + a) / 2)
#         value = nodes[mid]
#         if required == value:
#             predecessor = nodes[mid-1] if mid-1 >= 0 else None
#             successor = nodes[mid+1] if size-1 > mid else None
#             return predecessor, successor
#         elif required > value:
#             a = mid
#         else:
#             b = mid

#     predecessor = nodes[a]
#     successor = nodes[a+1]
#     return predecessor, successor


# def inorder_predecessor_and_successor(root: Node, required: any, _answer: list = []) -> Tuple[Node, Node]:
#     if isinstance(root.left, Node):
#         inorder_predecessor_and_successor(root.left, required, _answer)

#     if root.data == required:
#         predecessor = _answer[-1]
#         successor = left_most(root.right)
#         return predecessor, successor

#    _answer.append(root.data)

#     if isinstance(root.right, Node):
#         inorder_predecessor_and_successor(root.right, required, _answer)

#     return _answer


def inorder_predecessor_and_successor(root: Node, required: any) -> Tuple[Node, Node]:
    if root.data == required and isinstance(root.left, Node) and isinstance(root.right, Node):
        predecessor = right_most(root.left)
        successor = left_most(root.right)
        return predecessor, successor

    if isinstance(root.left, Node):
        inorder_predecessor_and_successor(root.left, required)

    if root.data == required:
        predecessor = inorder_predecessor_and_successor.buffer
        successor = left_most(root.right)
        return predecessor, successor

    inorder_predecessor_and_successor.buffer = root.data

    if isinstance(root.right, Node):
        inorder_predecessor_and_successor(root.right, required)


if __name__ == '__main__':
    _1 = Node(1)
    _3 = Node(3)
    _4 = Node(4)
    _6 = Node(6)
    _7 = Node(7)
    _8 = Node(8)
    _10 = Node(10)
    _13 = Node(13)
    _14 = Node(14)

    _8.left = _3
    _8.right = _10
    _3.left = _1
    _3.right = _6
    _6.left = _4
    _6.right = _7
    _10.right = _14
    _14.left = _13

    print(inorder_predecessor_and_successor(_8, 8))