#include <bits/stdc++.h>
using namespace std;


int coin_change(vector<int> &coins, int k, int i)
{
    if (k < 0)
    {
        return 0;
    }
    if (k == 0)
    {
        return 1;
    }

    if (i < 0)
    {
        return 0;
    }

    return coin_change(coins, k-coins[i], i) + coin_change(coins, k, i-1);
}

int count_ways_coin_change(vector<int> &coins, int k)
{
    return coin_change(coins, k, coins.size()-1);
}


int main()
{
    vector<int> coins;

    coins = {1, 2, 3};
    cout << count_ways_coin_change(coins, 4) << endl;

    coins = {2, 5, 3, 6};
    cout << count_ways_coin_change(coins, 10) << endl;

    return 0;
}