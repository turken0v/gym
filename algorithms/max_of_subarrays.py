from data_structures.deque import Deque
from typing import List

# def max_of_subarrays(array: List[int], k: int) -> List[int]:
#     answer = []
#     size = len(array)
#     i = 0
#     while size >= i + 3:
#         subarray = array[i:i+3]
#         max_of = max(subarray)
#         answer.append(max_of)
#         i += 1

#     return answer


def max_of_subarrays(array: List[int], k: int) -> List[int]:
    deq = Deque()

    for i in range(k):
        while deq and array[i] > array[deq.front]:
            deq.pop()

        deq.append(i)

    answer = []
    size = len(array)
    for i in range(k, size):
        answer.append(array[deq.rear])

        while deq and i-k >= deq.rear:
            deq.popleft()

        while deq and array[i] > array[deq.front]:
            deq.pop()

        deq.append(i)

    answer.append(array[deq.rear])

    return answer


if __name__ == '__main__':
    print(
        max_of_subarrays([
            1, 2, 3, 1, 4, 5, 2, 3, 6
        ], k=3)
    )