from data_structures.binary_tree import Node
from data_structures.deque import Deque


class Node(Node):
    next_right: Node = None


# O(n)
def connect_nodes_at_same_level(root: Node) -> None:
    root.next_right = None
    qelement = root, 0
    deq = Deque([qelement])
    while not deq.is_empty():
        node, level = deq.popleft()

        if not deq.is_empty():
            next_right_node, next_right_level = deq.rear
            if level == next_right_level:
                node.next_right = next_right_node
        else:
            node.next_right = None

        if isinstance(node.left, Node):
            qelement = node.left, level+1
            deq.append(qelement)

        if isinstance(node.right, Node):
            qelement = node.right, level+1
            deq.append(qelement)


if __name__ == '__main__':
    a = Node('A')
    b = Node('B')
    c = Node('C')
    d = Node('D')
    e = Node('E')
    f = Node('F')

    a.left = b
    a.right = c
    b.left = d
    b.right = e
    c.right = f

    connect_nodes_at_same_level(a)