#include <iostream>
#include <string>
using namespace std;


#define MAX_SYMBOLS 26


struct SuffixTreeNode
{
    SuffixTreeNode *childs[MAX_SYMBOLS];
    int l, *r;
    SuffixTreeNode *link;

    SuffixTreeNode(int l, int *r, SuffixTreeNode *link = NULL)
    {
        this->l = l;

        this->r = r;

        this->link = link;

        for (int i = 0; i < MAX_SYMBOLS; i++)
        {
            this->childs[i] = NULL;
        }
    }
};


typedef SuffixTreeNode Node;


int symbol_number(char x)
{
    return x - 'a';
}


Node* build_suffix_tree(string text)
{
    int *end = new int(0);

    Node *root = new Node(0, end),
         *node = root;

    int i = 0, j = 0;
    while (i < text.length())
    {
        int num = symbol_number(text[i]);

        if (node->childs[num] != NULL)
        {
            node = node->childs[num];
            j++;
        }
        else if (node != root && text[node->l+j] == text[i])
        {
            j++;
        }
        else
        {
            if (j > 0)
            {
                node->childs[num] = new Node(i, end);

                node->r = new int(node->l+j);

                num = symbol_number(text[*node->r]);

                node->childs[num] = new Node(*node->r, end);

                j--;

                if (node->link == NULL)
                {
                    num = symbol_number(text[*node->r-j]);
                    node->link = root->childs[num];
                }

                node = node->link;

                continue;
            }

            node->childs[num] = new Node(i, end);
        }

        i++;
        (*end)++;
    }

    return root;
}


void print_suffix_tree(Node *node, string text, string v = "")
{
    int length = *node->r - node->l;
    v += text.substr(node->l, length);
    cout << v << endl;

    for (int i = 0; i < MAX_SYMBOLS; i++)
    {
        if (node->childs[i] != NULL)
        {
            print_suffix_tree(node->childs[i], text, v + "->");
        }
    }
}


int main()
{
    string text = "bananas";
    Node *root = build_suffix_tree(text);
    root->r = new int(0);
    print_suffix_tree(root, text);

    return 0;
}