from data_structures.binary_tree import Node, print_inorder_binary_tree
from typing import List


# O(n^2)
def build_binary_tree(preorder: List, inorder: List):
    value = preorder.pop(0)
    node = Node(value)
    i = inorder.index(value)
    left = inorder[:i]
    right = inorder[i+1:]

    try:
        next_value = preorder[0]
        if next_value in left:
            node.left = build_binary_tree(preorder, left)
    except:
        pass

    try:
        next_value = preorder[0]
        if next_value in right:
            node.right = build_binary_tree(preorder, right)
    except:
        pass

    return node


if __name__ == '__main__':
    inorder  = [4,3,5,2,7,6,8,1,12,10,13,9,14,11,15]
    preorder = [1,2,3,4,5,6,7,8,9,10,12,13,11,14,15]
    root = build_binary_tree(preorder, inorder)
    print_inorder_binary_tree(root)