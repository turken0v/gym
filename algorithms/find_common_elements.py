from algorithms.searching_and_sorting.merge_sort import merge
from typing import List


def find_common_elements_in_sorted(*args) -> List:
    elements = []
    for array in args:
        elements = merge(elements, array)

    answer = []
    k = len(args)
    counter = 0
    prev = None
    for value in elements:
        if value == prev:
            counter += 1

            if counter >= k-1:
                answer.append(value)
        else:
            counter = 0

        prev = value

    return answer

if __name__ == '__main__':
    a = [1, 5, 10, 20, 40, 80]
    b = [6, 7, 20, 80, 100]
    c = [3, 4, 15, 20, 30, 70, 80, 120]
    print(find_common_elements_in_sorted(a, b, c))