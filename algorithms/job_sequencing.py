from heapq import _heapify_max as heapify, _heappop_max as hpop


def job_sequencing(DL, P):
    n = len(DL)
    if n != len(P):
        raise ValueError

    A = [(P[i], DL[i]) for i in range(n)]
    heapify(A)
    print(A)

    TL = set()
    J = {}
    for _ in range(n):
        p, dl = hpop(A)
        for i in range(dl, 0, -1):
            if i not in TL:
                TL.add(i)
                J[i] = p
                break

    return TL, J


if __name__ == '__main__':
    DL = [2, 1, 2, 3, 3]
    P  = [15, 10, 20, 5, 1]
    print(job_sequencing(DL, P))
