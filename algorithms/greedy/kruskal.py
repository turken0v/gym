from algorithms.detect_cycle import is_cycled_undirecred
from typing import List, Tuple


def take_weight(edge_with_weight):
    return edge_with_weight[-1]

def min_spanning_tree(vertices: List, EW: List[Tuple]) -> List[Tuple]:
    n = len(vertices) - 1
    sorted_edges = [(u, v) for u, v, _ in sorted(EW, key=take_weight)]

    path = []
    i = 0
    while n > i:
        edge = sorted_edges.pop(0)

        if not is_cycled_undirecred(vertices, path + [edge]):
            path.append(edge)
            i += 1

    return path


if __name__ == '__main__':
    V = (0,1,2,3,4,5,6,7,8,)
    E = [
        (0, 1, 4),
        (0, 7, 8),
        (1, 2, 8),
        (1, 7, 11),
        (7, 8, 7),
        (7, 6, 1),
        (2, 3, 7),
        (2, 5, 4),
        (2, 8, 2),
        (6, 8, 6),
        (6, 5, 2),
        (3, 4, 9),
        (3, 5, 14),
        (5, 4, 10)
    ]
    print(min_spanning_tree(V, E))