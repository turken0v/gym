from datetime import time
from typing import List, Tuple

def as_minutes(timeline: List[Tuple]) -> List[Tuple]:
    answer = []
    for a, b in timeline:
        c = a.hour * 60 + a.minute
        d = b.hour * 60 + b.minute
        _ = c, d
        answer.append(_)

    return answer

# O(n^2)
# def find_min_num_of_platforms4railway(timetable: List[Tuple]) -> int:
#     T = as_minutes(timetable)
#     k = 0
#     i = 0
#     for a, b in T:
#         for c, d in T:
#             if a <= c <= b or a <= d <= b:
#                 i += 1

#         if i > k:
#             k = i

#         i = 0

#     return k


# O(1440n)
# def find_min_num_of_platforms4railway(timetable: List[Tuple]) -> int:
#     T = as_minutes(timetable)
#     timeline = [0] * 1440
#     for a, b in T:
#         i = 0
#         while b-a+1 > i:
#             timeline[a+i] += 1
#             i += 1

#     return max(timeline)


def find_min_num_of_platforms4railway(timetable: List[Tuple]) -> int:
    T = as_minutes(timetable)
    size = len(T)
    n = 0
    for i, (a, b) in enumerate(T):
        i += 1
        k = 0
        while size > i:
            c = T[i][0]
            d = T[i][1]

            if not a <= c <= b or not a <= d <= b:
                break

            k += 1
            if k > n:
                n = k

            i += 1

    return n+1



if __name__ == '__main__':
    timeline = [
        (time(hour=9, minute=0), time(hour=9, minute=10)),
        (time(hour=9, minute=40), time(hour=12, minute=0)),
        (time(hour=9, minute=50), time(hour=11, minute=20)),
        (time(hour=11, minute=0), time(hour=11, minute=30)),
        (time(hour=15, minute=0), time(hour=19, minute=0)),
        (time(hour=18, minute=0), time(hour=20, minute=0))
    ]
    num = find_min_num_of_platforms4railway(timeline)
    print(num)