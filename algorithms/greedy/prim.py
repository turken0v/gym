from heapq import heapify, heappop as hpop, heappush as hpush
from typing import List


def min_spanning_tree(EW: List, first=0):
    directions = {}
    weights = {}
    buffer = []
    explored = {}
    for u, v, w in EW:
        if u == first:
            explored[u] = True
            _ = w, v, (u, v)
            buffer.append(_)

        directions[u] = directions.get(u, []) + [v]
        directions[v] = directions.get(v, []) + [u]
        _ = u, v
        weights[_] = w
        _ = v, u
        weights[_] = w

    answer = []
    heapify(buffer)
    n = len(directions)
    i = 0
    while buffer and n > i:
        w, u, edge = hpop(buffer)

        if explored.get(u, None) == True:
            continue

        explored[u] = True

        answer.append(edge)

        vertices = directions.pop(u, [])
        for v in vertices:
            if explored.get(v, None) is None:
                _ = u, v
                __ = weights[_], v, _
                hpush(buffer, __)

        i += 1

    return answer


if __name__ == '__main__':
    # V = (0,1,2,3,4,5,6,7,8,)
    EW = [
        (0, 1, 4),
        (0, 7, 8),
        (1, 2, 8),
        (1, 7, 11),
        (7, 8, 7),
        (7, 6, 1),
        (2, 3, 7),
        (2, 5, 4),
        (2, 8, 2),
        (6, 8, 6),
        (6, 5, 2),
        (3, 4, 9),
        (3, 5, 14),
        (5, 4, 10)
    ]
    print(min_spanning_tree(EW))