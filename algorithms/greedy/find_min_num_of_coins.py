def find_min_num_of_coins(C, value):
    A = []
    E = sorted(C, reverse=True)
    n = len(E)

    last = 0
    while value > 0:
        for i in range(n-last):
            i += last
            c = E[i]

            if value >= c:
                value -= c
                A.append(c)
                last = i
                break

    return A


if __name__ == '__main__':
    C = set([1, 2, 5, 10, 20, 50, 100, 500, 1000])
    print(find_min_num_of_coins(C, 221))