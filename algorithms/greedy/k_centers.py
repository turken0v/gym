from algorithms.arg_funcs import arg_min
from statistics import median
from typing import List, Tuple, Dict


def search_closest(data: Dict, required: any) -> any:
    abs_with_key = []
    for key in data.keys():
        delta = abs(required - key)
        _ = delta, key
        abs_with_key.append(_)

    _, i = min(abs_with_key)
    return data[i]

def find_k_centers(points: List[Tuple], k: int, xmax: int, ymax: int) -> List[Tuple]:
    map = {}
    c = xmax * ymax / k
    for _ in points:
        x, y = _
        m = x * y
        i = m // c
        
        submap = map.get(i, {})
        submap[m] = submap.get(m, []) + [_]
        map[i] = submap

    answer = []
    for submap in map.values():
        M = sorted([m for m in submap.keys()])
        avg = median(M)
        points = search_closest(submap, avg)
        answer.append(points)

    return answer


if __name__ == '__main__':
    points = [
        (2, 5),
        (20, 5),
        (28, 5),
        (10, 10),
        (22, 10),
        (28, 10),
        (14, 12),
        (8, 15),
        (14, 15),
        (17, 15),
        (30, 15),
        (9, 20)
    ]
    print(find_k_centers(points, 3, 30, 20))