from heapq import _heapify_max as heapify, _heappop_max as hpop


def job_sequencing(DL, P):
    n = len(DL)
    if n != len(P):
        raise ValueError

    A = [(P[i], DL[i]) for i in range(n)]
    heapify(A)

    TL = []
    J = []
    while A:
        p, dl = hpop(A)
        for i in range(dl, 0, -1):
            if i not in TL:
                TL.append(i)
                J.append(p)
                break

    return TL, J


if __name__ == '__main__':
    DL = [2, 1, 2, 1, 3]
    P  = [100, 19, 27, 25, 15]
    print(job_sequencing(DL, P))
