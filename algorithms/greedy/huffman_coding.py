from heapq import heapify, heappop as hpop, heappush as hpush
from data_structures.deque import Deque
from typing import Tuple, List

# # O(nlogn)
# def huffman_coding(V: List[Tuple[int, str]]):
#     A = V[:]
#     B = []
#     C = []
#     heapify(A)

#     def min_frequency(A: List[Tuple[int, str]], B: List[Tuple[int, str]]):
#         if len(B) == 0:
#             return hpop(A)

#         if len(A) == 0:
#             return hpop(B)

#         if A[0][0] > B[0][0]:
#             return hpop(B)

#         return hpop(A)

#     i = 1
#     N = None
#     while A:
#         left = min_frequency(A, B)
#         right = min_frequency(A, B)

#         freq = left[0] + right[0]
#         value = 'N' + str(i)
#         N = freq, value
#         hpush(B, N)
#         i += 1

#         hpush(C, left)
#         hpush(C, right)

#     hpush(C, N)

#     return C

# Input must be sorted!
# O(n)
def huffman_coding(V: List[Tuple[int, str]]):
    A = Deque(V[:])
    B = Deque()

    def min_frequency(left: Deque, right: Deque):
        if len(right) == 0:
            return left.pop()

        if len(left) == 0:
            return right.pop()

        if right.rear[0] > left.rear[0]:
            return left.pop()

        return right.pop()

    answer = []
    while A:
        left = min_frequency(A, B)
        right = min_frequency(A, B)

        node = left[0] + right[0], left + right
        B.append(node)
        answer.append(node)

    return answer


if __name__ == '__main__':
    V = [
        (5, 'a'),
        (9, 'b'),
        (12, 'c'),
        (13, 'd'),
        (16, 'e'),
        (45, 'f'),
    ]
    print(huffman_coding(V))