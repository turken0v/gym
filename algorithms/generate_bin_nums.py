from data_structures.deque import Deque
from typing import List

# def generate_bin_nums(num: int) -> List[int]:
#     answer = []

#     counter = 1
#     while num >= counter:
#         prefix = bin(counter)[2:]
#         answer.append(prefix)
#         counter += 1

#     return answer


# # O(n)
# def generate_bin_nums(num: int) -> List[int]:
#     answer = []

#     counter = 0
#     while num > counter:
#         prefix = 10 ** counter
#         answer.append(prefix)
#         combs_count = (2 ** counter) - 1

#         for i in range(combs_count):
#             comb = prefix + answer[i]
#             answer.append(comb)
#             counter += 1

#         counter += 1 if combs_count == 0 else 0

#     return answer[:num]


# O(n)
def generate_bin_nums(num: int) -> List[int]:
    answer = []
    deq = Deque()
    counter = 0
    while num > 0:
        deq.appendleft(0)

        prefix = 10 ** counter
        while deq:
            postfix = deq.popleft()
            comb = prefix + postfix
            answer.append(comb)
            num -= 1

        deq = Deque(answer)
        counter += 1

    return answer[:num]


if __name__ == '__main__':
    print(generate_bin_nums(5))