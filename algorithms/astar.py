import math


def dist(p1, p2):
    """
    Euclidean distance
    """
    x = p2[0] - p1[0]
    y = p2[1] - p1[1]
    return math.sqrt(x**2 + y**2)


def astar():
    pass


if __name__ == '__main__':
    # M = (
    #     0,  10, 5,  0,  0,
    #     10,  0, 0,  8,  0,
    #     5,   0, 0,  8,  0,
    #     0,   8, 8,  0, 15,
    #     0,   0, 0, 15,  0
    # )

    V = set([1, 2, 3, 4, 5])
    E = (
        (1, 2), (1, 3),
        (2, 4),
        (3, 4),
        (4, 5),
    )
    # W = (
    #     10, 5,
    #     8,
    #     14,
    #     15
    # )