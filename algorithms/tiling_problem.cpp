#include <bits/stdc++.h>
using namespace std;


// void count_ways(int n, int x, int y, unordered_map<string, int> &output, int h = 0, int v = 0) {
//     bool status = true;

//     if (y == 0 && n > x) {
//         // horizontal
//         count_ways(n, x+1, y, output, h+1, v);
//         status = false;
//     }

//     if (n >= x+2) {
//         // vertical
//         if (y < 1) {
//             count_ways(n, x, y+1, output, h, v+1);
//         } else {
//             count_ways(n, x+2, 0, output, h, v+1);
//         }

//         status = false;
//     }

//     if (status) {
//         string key = to_string(h) + '#' + to_string(v);
//         output[key]++;
//     }
// }


// int count_ways_to_tile_board(int n) {
//     unordered_map<string, int> output;
//     count_ways(n, 0, 0, output, 0, 0);

//     int counter = output.size();
//     return counter;
// }


// int main() {
//     cout << count_ways_to_tile_board(8) << endl;

//     return 0;
// }

// #include <iostream>
// using namespace std;
 
// int getNoOfWays(int n)
// {
//     // Base case
//     if (n == 0)
//         return 0;
//     if (n == 1)
//         return 1;
 
//     return getNoOfWays(n - 1) + getNoOfWays(n - 2);
// }
 
// // Driver Function
// int main()
// {
//     cout << getNoOfWays(4) << endl;
//     cout << getNoOfWays(3) << endl;
//     cout << getNoOfWays(8) << endl;
//     return 0;
// }

void count_ways(int n, int x, int y) {
    if (y+2 <= 3) {
        count_ways(n, x, y+2);
    }

    if (n > x) {
        count_ways(n, x+1, y);
    }

    if (n >= x+2) {
        // vertical
        if (y < 2) {
            count_ways(n, x, y+1);
        } else {
            count_ways(n, x+2, 0);
        }
    }
}


int count_ways_to_tile_board(int n) {
    unordered_map<string, int> output;
    count_ways(n, 0, 0, output, 0, 0);

    int counter = output.size();
    return counter;
}


int main() {
    cout << count_ways_to_tile_board(8) << endl;

    return 0;
}