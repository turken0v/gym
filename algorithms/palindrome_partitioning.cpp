#include <bits/stdc++.h>
using namespace std;


bool is_palindrome(string input) {
    int n = input.length();
    for (int i = 0; i < n/2; i++) {
        if (input[i] != input[n-i-1]) {
            return false;
        }
    }

    return true;
}

int partitioning(string I) {
    if (is_palindrome(I)) {
        return 0;
    }

    string A, B;
    int count = I.length()-1;
    for (int i = 1; i < I.length()-1; i++) {
        A = I.substr(0, i);
        B = I.substr(i);

        count = min(count, partitioning(A) + partitioning(B) + 1);
    }

    return count;
}


int palindrome_partion(string I) {
    int n = I.length(), i, l, r;

    int T[n][n];
    for (i = 0; i < n; i++) {
        T[i][i] = 0;
    }

    for (i = 2; i <= n; i++) {
        for (l = 0; l < n-i+1; l++) {
            r = l+i-1;

            if (i == 2) {
                if (I[l] == I[r]) {
                    T[l][r] = 0;
                } else {
                    T[l][r] = 1;
                }

                continue;
            }

            if (I[l] == I[r]) {
                if (T[l+1][r-1] == 0) {
                    T[l][r] = 0;
                    continue;
                }
            }

            T[l][r] = r-l;
            for (int k = l; k < r; k++) {
                T[l][r] = min(T[l][r], T[l][k] + T[k+1][r] + 1);
            }
        }
    }

    return T[0][n-1];
}


int main() {
    string input;

    // input = "ababbbabbababa";
    // cout << partitioning(input) << endl;

    input = "ababbbabbababa";
    cout << palindrome_partion(input) << endl;

    input = "geek";
    cout << palindrome_partion(input) << endl;

    input = "aaaa";
    cout << palindrome_partion(input) << endl;

    input = "abcde";
    cout << palindrome_partion(input) << endl;

    input = "abbac";
    cout << palindrome_partion(input) << endl;

    return 0;
}