# Find all possible words in a board of characters
from data_structures.stack import Stack
from itertools import product
from typing import List


def is_word_in_board(board: List, word: str):
    size = len(word)
    rows = len(board)
    cols = len(board[0])

    element = 0, 0, 0
    buffer = Stack([element])
    while buffer:
        x, y, i = buffer.pop()
        character = board[y][x]
        required = word[i]

        if character == required:
            i += 1

            if i >= size:
                return True

            directions = (x-1, x, x+1), (y-1, y, y+1)
            for a, b in product(*directions):
                if cols > a >= 0 and rows > b >= 0:
                    if a == x and b == y:
                        continue

                    element = a, b, i
                    buffer.append(element)
        elif i == 0:
            x += 1
            if x >= cols:
                y += 1
                x = 0

            if rows > y >= 0:
                element = x, y, 0
                buffer.append(element)

    return False


if __name__ == '__main__':
    board = [
        ['G', 'I', 'Z'],
        ['U', 'E', 'K'],
        ['Q', 'S', 'E'],
    ]
    print(is_word_in_board(board, 'GO'))