#include <bits/stdc++.h>
using namespace std;


int LCS(string A, string B) {
    int n = A.length(), m = B.length();

    int T[m+1][n+1];
    memset(T, 0, sizeof(int)*(m+1)*(n+1));

    for (int i = 1; i < m+1; i++) {
        for (int j = 1; j < n+1; j++) {
            if (B[i-1] == A[j-1]) {
                T[i][j] = T[i-1][j] + 1;
            } else {
                T[i][j] = max(T[i-1][j], T[i][j-1]);
            }
        }
    }

    return m - T[m][n];
}


int main() {
    cout << "Edit distance: " << LCS("geek", "gesek") << endl;
    cout << "Edit distance: " << LCS("cat", "cut") << endl;
    cout << "Edit distance: " << LCS("SUNDAY", "SATURDAY") << endl;

    return 0;
}