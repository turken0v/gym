from catalan_num import catalan_num
from factorial import factorial

def num_labeled_trees(n: int):
    """
    How many different Unlabeled Binary Trees can be there with n nodes?
    Answer: catalan_num(n)

    num_labeled_tress(n) = catalan_num(n) * factorial(n)
    """
    return catalan_num(n) * factorial(n)

if __name__ == '__main__':
    print(num_labeled_trees(3))