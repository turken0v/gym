from topological_sorting import sort_topological
from detect_cycle import has_cycle_directed
from typing import List, Tuple


def is_before(a: any, b: any, array: List[any]) -> bool:
    return array.index(b) > array.index(a)



def assign_acyclic_directions(directed: List[Tuple[any, any]], undirected: List[Tuple[any, any]]) -> List[Tuple[any, any]]:
    assert not has_cycle_directed(directed), ValueError
    sorted_vertices = list(sort_topological(directed))

    for u, v in undirected:
        edge = (u, v) if is_before(u, v, sorted_vertices) else (v, u)

        directed.append(edge)



if __name__ == '__main__':
    directed = [
        (0, 1),
        (0, 5),
        (5, 1),
        (5, 2),
        (1, 2),
        (1, 3),
        (1, 4),
        (2, 3),
        (2, 4),
        (3, 4),
    ]
    undirected = [
        (0, 2),
        (3, 0),
        (5, 4),
    ]
    assign_acyclic_directions(directed, undirected)
    print(directed)