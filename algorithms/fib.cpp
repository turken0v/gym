#include <bits/stdc++.h>
using namespace std;

int fib(int n)
{
    int T[n];
    T[0] = 0;
    T[1] = 1;
    for (int i = 2; i < n; i++)
    {
        T[i] = T[i-1] + T[i-2];
    }

    return T[n-1];
}

int main()
{
    cout << fib(3) << endl;
    cout << fib(4) << endl;
    cout << fib(5) << endl;
    cout << fib(6) << endl;
    cout << fib(7) << endl;
    cout << fib(8) << endl;
    cout << fib(9) << endl;
    return 0;
}