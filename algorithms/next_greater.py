from data_structures.stack import Stack
from typing import List


# def next_greater(E: List[float]) -> List[float]:
#     s = len(E)
#     R = []
#     for i in range(s):
#         k = -1
#         for j in range(i+1, s):
#             if E[j] > E[i]:
#                 k = E[j]
#                 break

#         R.append(k)

#     return R


def next_greater(elements: List[float]) -> List[float]:
    answer = []

    first = elements[0]
    S = Stack([first])
    size = len(elements)
    for i in range(1, size):
        value = elements[i]

        while not S.is_empty() and value > S.top:
            top = S.pop()
            answer.append((top, value))

        S.append(value)

    while not S.is_empty():
        remainder = S.pop()
        answer.append((remainder, -1))

    return answer

if __name__ == '__main__':
    print(next_greater([2,3,1,4]))