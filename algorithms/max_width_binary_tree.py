from data_structures.binary_tree import Node
from data_structures.deque import Deque

# def binary_tree_max_width(root: Node) -> int:
#     if root is None:
#         return 0

#     max_at_level = {}
#     first_element = root, 1
#     q = Deque([first_element])
#     while q:
#         node, level = q.popleft()

#         if isinstance(node.left, Node):
#             value = max_at_level.get(level, 0)
#             max_at_level[level] = value + 1

#             element = node.left, level+1
#             q.append(element)

#         if isinstance(node.right, Node):
#             value = max_at_level.get(level, 0)
#             max_at_level[level] = value + 1

#             element = node.right, level+1
#             q.append(element)

#     levels_width = max_at_level.values()
#     return max(levels_width)


def _binary_tree_max_width(root: Node, level, max_at_level):
    width = 0
    if isinstance(root.left, Node):
        width += 1
        _binary_tree_max_width(root.left, level+1, max_at_level)

    if isinstance(root.right, Node):
        width += 1
        _binary_tree_max_width(root.right, level+1, max_at_level)

    value = max_at_level.get(level, 0)
    max_at_level[level] = value + width

    return max_at_level

def get_binary_tree_max_width(root: Node) -> int:
    if root is None:
        return 0

    max_at_level = {0: 1}
    _binary_tree_max_width(
        root,
        level=1,
        max_at_level=max_at_level
    )

    levels_width = max_at_level.values()
    return max(levels_width)

if __name__ == '__main__':
    _1 = Node(1)
    _2 = Node(2)
    _3 = Node(3)
    _4 = Node(4)
    _5 = Node(5)
    _6 = Node(6)
    _7 = Node(7)
    _8 = Node(8)

    _1.left = _2
    _1.right = _3
    _2.left = _4
    _2.right = _5
    _3.right = _8
    _8.left = _6
    _8.right = _7

    print(get_binary_tree_max_width(_1))