from typing import List
from data_structures.deque import Deque


def find_longest_path(edges_with_weights: List, first_point = 0) -> List:
    buffer = Deque()
    directions = {}
    for u, v, w in edges_with_weights:
        if u == first_point:
            qelement = v, w, [u]
            buffer.append(qelement)

        directions[u] = directions.get(u, []) + [(v, w)]

    longest_path = []
    max_len = 0
    while not buffer.is_empty():
        u, w, path = buffer.popleft()

        if w > max_len:
            longest_path = path+[u]
            max_len = w

        vertices = directions.get(u, None)
        if vertices:
            for vertex in vertices:
                v, vw = vertex
                qelement = v, w+vw, path+[u]
                buffer.append(qelement)

    return longest_path


if __name__ == '__main__':
    edges_with_weights = [
        (0, 1, 5),
        (0, 2, 3),
        (1, 2, 2),
        (1, 3, 6),
        (2, 4, 4),
        (2, 5, 2),
        (2, 3, 7),
        (3, 5, 1),
        (3, 4, -1),
        (4, 5, -2),
    ]
    print(find_longest_path(edges_with_weights))