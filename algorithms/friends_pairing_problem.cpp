#include <bits/stdc++.h>
using namespace std;


int factorial(int n) {
    int answer = 1;
    for (int i = 2; i <= n; i++) {
        answer *= i;
    }

    return answer;
}

int C(int n, int k) {
    return factorial(n) / (factorial(k) * factorial(n-k));
}


int count_pairs(int n) {
    int counter = 1;
    for (int i = 2; i < n; i++) {
        counter += C(n, i);
    }

    return counter;
}


int main() {
    cout << count_pairs(4) << endl;
    cout << count_pairs(5) << endl;
    cout << count_pairs(6) << endl;

    return 0;
}