from data_structures.binary_tree import Node
from typing import Tuple


def get_floor_and_ceil(root: Node, value) -> Tuple:
    def _f(root: Node, value) -> None:
        if isinstance(root.left, Node):
            _f(root.left, value)

        if root.data == value:
            _f.floor = root.data
            _f.ceil = root.data
        elif _f.ceil == -1 and root.data > value:
            _f.ceil = root.data
        elif value > root.data:
            _f.floor = root.data

        if isinstance(root.right, Node):
            _f(root.right, value)

    setattr(_f, 'floor', -1)
    setattr(_f, 'ceil', -1)
    _f(root, value)
    return _f.floor, _f.ceil

if __name__ == '__main__':
    _8 = Node(8)
    _4 = Node(4)
    _12 = Node(12)
    _2 = Node(2)
    _6 = Node(6)
    _10 = Node(10)
    _14 = Node(14)

    _8.left = _4
    _8.right = _12
    _4.left = _2
    _4.right = _6
    _12.left = _10
    _12.right = _14

    print(get_floor_and_ceil(_8, 1))