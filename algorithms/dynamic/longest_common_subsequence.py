from typing import Sequence


def lcs(X: Sequence, Y: Sequence) -> Sequence:
    m = len(X)
    n = len(Y)

    L = [[0]+[-1]*(n) for i in range(m+1)]

    for i in range(1, m+1):
        for j in range(1, n+1):
            if X[i-1] == Y[j-1]:
                L[i][j] = L[i-1][j-1]+1
            else:
                L[i][j] = max(L[i-1][j], L[i][j-1])

    L[m].remove(0)

    return L[m]


def lcs_length(X: Sequence, Y: Sequence) -> int:
    _ = lcs(X, Y)
    return max(_)

if __name__ == '__main__':
    a = 'ABCDGH'
    b = 'AEDFHR'
    print(lcs_length(a, b))
    a = 'AGGTAB'
    b = 'GXTXAYB'
    print(lcs_length(a, b))