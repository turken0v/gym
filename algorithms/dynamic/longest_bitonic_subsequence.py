from algorithms.dynamic.longest_increasing_subsequence import arg_LIS
from algorithms.dynamic.longest_decreasing_subsequence import arg_LDS
from typing import Sequence


def LBS(S: Sequence) -> int:
    I = arg_LIS(S)
    D = arg_LDS(S)
    while I[-1] >= D[0]:
        D.pop(0)

    return [S[i] for i in I + D]


if __name__ == '__main__':
    a = [1, 11, 2, 10, 4, 5, 2, 1]
    print(LBS(a))