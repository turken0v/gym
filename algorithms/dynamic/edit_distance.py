from algorithms.dynamic.longest_common_subsequence import lcs
from typing import Sequence, Tuple


def edit_distance_string(a: Sequence, b: Sequence) -> int:
    differences = lcs(a, b)
    n = len(differences)
    counter = 1 if differences[0] == 0 else 0
    for i in range(1, n):
        if differences[i] == differences[i-1]:
            counter += 1

    return counter


if __name__ == '__main__':
    a = "sunday"
    b = "saturday"
    print(edit_distance_string(a, b))
    a = "cat"
    b = "cut"
    print(edit_distance_string(a, b))
    a = "geek"
    b = "gesek"
    print(edit_distance_string(a, b))
