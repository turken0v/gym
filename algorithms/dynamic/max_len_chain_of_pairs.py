# https://www.geeksforgeeks.org/maximum-length-chain-of-pairs-dp-20/
from typing import List, Tuple


def max_len_chain_of_pairs(pairs: List[Tuple]) -> int:
    n = len(pairs)
    T = [0] * n
    for i in range(1, n):
        for j in range(i):
            a = pairs[i][1]
            b = pairs[j][0]
            if a > b and T[j]+1 > T[i]:
                T[i] = T[j] + 1

    return max(T)


if __name__ == '__main__':
    a = [
        (5, 24), 
        (39, 60), 
        (15, 28), 
        (27, 40), 
        (50, 90)
    ]
    print(max_len_chain_of_pairs(a))