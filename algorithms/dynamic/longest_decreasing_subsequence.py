from typing import List
from algorithms.arg_funcs import arg_max


def arg_LDS(S: List) -> int:
    n = len(S)
    T = [0] * n
    for i in range(1, n):
        for j in range(i):
            if S[j] > S[i] and T[j]+1 > T[i]:
                T[i] = T[j] + 1

    i, k = arg_max(T)
    answer = [i]
    k -= 1
    while k >= 0:
        i = answer[0]
        smallest = None
        for j in range(i, -1, -1):
            if T[j] == k and S[j] > S[i]:
                if smallest is None or S[smallest] > S[j]:
                    smallest = j

        answer.insert(0, smallest)
        k -= 1

    return answer


def LDS(S: List) -> int:
    n = len(S)
    T = [0] * n
    for i in range(1, n):
        for j in range(i):
            if S[j] > S[i] and T[j]+1 > T[i]:
                T[i] = T[j] + 1

    i, k = arg_max(T)
    answer = [ S[i] ]
    k -= 1
    bound = n
    while k >= 0:
        smallest = float('inf')
        for i in range(bound-1, -1, -1):
            if T[i] == k and smallest > S[i] and S[i] > answer[0]:
                smallest = S[i]
                bound = i

        answer.insert(0, smallest)
        k -= 1

    return answer


if __name__ == '__main__':
    a = [80, 60, 41, 50, 21, 33, 9, 22, 10]
    print(LDS(a))