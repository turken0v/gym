def is_palindrome(x: str):
    return x == x[-1]


# def min_palindrome(x: str, i: int = 0, j: int = None):
#     if j is None:
#         j = len(x)-1

#     if i >= j or is_palindrome(x[i:j+1]):
#         return 0

#     answer = float('inf')
#     for k in range(i, j):
#         count = min_palindrome(x, i, k) + min_palindrome(x, k+1, j) + 1
#         answer = min(answer, count)

#     return answer


def min_palindrome(x: str) -> int:
    n = len(x)
    P = [[False for __ in range(n)] for _ in range(n)]
    C = [[0 for __ in range(n)] for _ in range(n)]

    for i in range(n):
        P[i][i], C[i][i] = True, 0

    for l in range(2, n+1):
        for i in range(n - l + 1):
            j = i + l - 1

            p = x[i] == x[j]
            if l != 2 and p:
                p = P[i+1][j-1]

            c = 0
            if not p:
                c = float('inf')
                for k in range(i, j):
                    _ = C[i][k] + C[k+1][j] + 1
                    c = min(c, _)

            P[i][j] = p
            C[i][j] = c

    return C[0][-1]


if __name__ == '__main__':
    a = "ababbbabbababa"
    print(min_palindrome(a))