from algorithms.arg_funcs import arg_max
from typing import Sequence


def MSIS(S: Sequence):
    n = len(S)
    T = [0] * n
    for i in range(1, n):
        for j in range(i):
            if S[i] >= S[j] and T[j]+1 > T[i]:
                T[i] = T[j]+1

    answer = -1
    for i in range(n-1, 0, -1):
        counter = S[i]
        for j in range(i):
            if S[i] >= S[j] and T[i] > T[j]:
                counter += S[j]

        if counter > answer:
            answer = counter

    return answer



if __name__ == '__main__':
    a = [1, 101, 2, 3, 100, 4, 5]
    print(MSIS(a))