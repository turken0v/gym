from typing import List


# O(n^2)
# def max_sum_contiguous_subarray(array: List) -> int:
#     n = len(array)
#     subarray = [0 for _ in range(n)]
#     answer = float('-inf')
#     for i in range(n-1):
#         subarray[i] = array[i]
#         for j in range(i+1, n):
#             subarray[j] = subarray[j-1] + array[j]
        
#         answer = max(subarray + [answer])

#     return answer


def max_sum_contiguous_subarray(array: List) -> int:
    answer = 0
    u = 0
    for v in array:
        u += v

        if u > answer:
            answer = u

        if u < 0:
            u = 0

    return answer


if __name__ == '__main__':
    a = [-2, -3, 4, -1, -2, 1, 5, -3]
    print(max_sum_contiguous_subarray(a))
    a = [1, 0, 5, 3, -1, 6, -4, 1]
    print(max_sum_contiguous_subarray(a))
    a = [-10, -1, 6, 9, -8, -10, -6, 6]
    print(max_sum_contiguous_subarray(a))