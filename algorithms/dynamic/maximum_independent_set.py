from data_structures.binary_tree import Node


def max_independent_set(root: Node) -> int:
    if root is None:
        return 0

    excluding_size = max_independent_set(root.left) + max_independent_set(root.right)

    including_size = 1
    if root.left:
        including_size += max_independent_set(root.left.left) + max_independent_set(root.right.right)

    if root.right:
        including_size += max_independent_set(root.right.left) + max_independent_set(root.right.right)

    return max(including_size, excluding_size)


if __name__ == '__main__':
    root = Node(20)  
    root.left = Node(8)  
    root.left.left = Node(4)  
    root.left.right = Node(12)  
    root.left.right.left = Node(10)  
    root.left.right.right = Node(14)  
    root.right = Node(22)  
    root.right.right = Node(25)
    print(max_independent_set(root))