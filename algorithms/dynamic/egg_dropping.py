from math import log2

def is_broken(k: int) -> bool:
    return k > 19


def find_floor(n: int, k: int, F) -> int:
    assert n > 0 and k > 0, ValueError

    l = 0
    r = k
    while not F(r):
        d = r - l
        l = r
        r += d - 1

    r = l
    while not F(r):
        r += 1

    return r


if __name__ == '__main__':
    print(find_floor(36, 10, is_broken))