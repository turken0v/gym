from typing import List
from algorithms.arg_funcs import arg_max


def arg_LIS(S: List) -> int:
    n = len(S)
    T = [0] * n
    for i in range(1, n):
        for j in range(i):
            if S[i] > S[j] and T[j]+1 > T[i]:
                T[i] = T[j] + 1

    i, k = arg_max(T)
    answer = [i]
    k -= 1
    while k >= 0:
        i = answer[0]
        largest = None
        for j in range(i, -1, -1):
            if T[j] == k and S[i] > S[j]:
                if largest is None or S[j] > S[largest]:
                    largest = j

        answer.insert(0, largest)
        k -= 1

    return answer


def LIS(S: List) -> int:
    n = len(S)
    T = [0] * n
    for i in range(1, n):
        for j in range(i):
            if S[i] > S[j] and T[j]+1 > T[i]:
                T[i] = T[j] + 1

    i, k = arg_max(T)
    k -= 1
    answer = [ S[i] ]
    bound = n
    while k >= 0:
        largest = float('-inf')
        for i in range(bound-1, -1, -1):
            if T[i] == k and S[i] > largest and answer[0] > S[i]:
                largest = S[i]
                bound = i

        answer.insert(0, largest)
        k -= 1

    return answer



if __name__ == '__main__':
    a = [10, 22, 9, 33, 21, 50, 41, 60, 80]
    print(LIS(a))
    a = [50, 3, 10, 7, 40, 80]
    print(LIS(a))