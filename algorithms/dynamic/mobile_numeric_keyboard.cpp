#include <iostream>
#include <vector>
using namespace std;

vector<vector<int>> NUM_KEYBOARD {
    {0,8},
    {1,2,4},
    {2,1,3,5},
    {3,2,6},
    {4,1,5,7},
    {5,2,4,6,8},
    {6,3,5,9},
    {7,4,8},
    {8,0,5,7,9},
    {9,6,8}
};


int count_possible_nums(int length)
{
    if (length == 1)
    {
        return 10;
    }
    else if (length < 1)
    {
        return 0;
    }

    int even[10];
    int odd[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    int status = 0;
    for (int i = 2; i <= length; i++)
    {
        status = 1 - status;

        if (status == 1)
        {
            even[0] = odd[0] + odd[8];
            even[1] = odd[1] + odd[2] + odd[4];
            even[2] = odd[2] + odd[1] + odd[3] + odd[5];
            even[3] = odd[3] + odd[2] + odd[6];
            even[4] = odd[4] + odd[1] + odd[5] + odd[7];
            even[5] = odd[5] + odd[2] + odd[4] + odd[6] + odd[8];
            even[6] = odd[6] + odd[3] + odd[5] + odd[9];
            even[7] = odd[7] + odd[4] + odd[8];
            even[8] = odd[8] + odd[0] + odd[5] + odd[7] + odd[9];
            even[9] = odd[9] + odd[6] + odd[8];
        }
        else
        {
            odd[0] = even[0] + even[8];
            odd[1] = even[1] + even[2] + even[4];
            odd[2] = even[2] + even[1] + even[3] + even[5];
            odd[3] = even[3] + even[2] + even[6];
            odd[4] = even[4] + even[1] + even[5] + even[7];
            odd[5] = even[5] + even[2] + even[4] + even[6] + even[8];
            odd[6] = even[6] + even[3] + even[5] + even[9];
            odd[7] = even[7] + even[4] + even[8];
            odd[8] = even[8] + even[0] + even[5] + even[7] + even[9];
            odd[9] = even[9] + even[6] + even[8];
        }
    }

    int total_count = 0;
    if (status == 1)
    {
        for (int i = 0; i < 10; i++)
        {
            total_count += even[i];
        }
    }
    else
    {
        for (int i = 0; i < 10; i++)
        {
            total_count += odd[i];
        }
    }

    return total_count;
}


int main()
{
    cout << count_possible_nums(1) << endl;
    cout << count_possible_nums(2) << endl;
    cout << count_possible_nums(3) << endl;
    cout << count_possible_nums(4) << endl;
    cout << count_possible_nums(5) << endl;
    cout << count_possible_nums(6) << endl;
    cout << count_possible_nums(7) << endl;
    return 0;
}