# best explanation https://www.youtube.com/watch?v=UflHuQj6MVA
from typing import Sequence


def lps(S: Sequence) -> int:
    n = len(S)

    P = [[0 for _ in range(n)] for _ in range(n)]

    for i in range(n):
        P[i][i] = 1

    for d in range(1, n):
        for i in range(n-d):
            j = i + d
            if S[i] == S[j]:
                P[i][j] = P[i+1][j-1] + 2
            else:
                P[i][j] = max(P[i][j-1], P[i+1][j])

    return P[0][-1]


if __name__ == '__main__':
    s = "GEEKS FOR GEEKS"
    print(lps(s))