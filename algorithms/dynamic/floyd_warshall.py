# https://www.geeksforgeeks.org/floyd-warshall-algorithm-dp-16/
from typing import List


INF = float('inf')

def floyd_warshall(graph: List[List]) -> List[List]:
    m = len(graph)
    D = graph[:]

    for k in range(m):
        for i in range(m):
            for j in range(m):
                if D[i][j] > D[i][k] + D[k][j]:
                    D[i][j] = D[i][k] + D[k][j]

    return D


if __name__ == '__main__':
    graph = [
        [0,   5,   INF, INF],
        [INF, 0,   3,   INF],
        [INF, INF, 0,   1  ],
        [INF, INF, INF, 0  ]
    ]
    print(floyd_warshall(graph))
    graph = [
        [0,   INF, -2,  INF],
        [4,   0,   3,   INF],
        [INF, INF, 0,   2  ],
        [INF, -1,  INF, 0  ]
    ]
    print(floyd_warshall(graph))