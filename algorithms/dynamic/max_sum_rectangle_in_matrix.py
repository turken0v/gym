from typing import List, Tuple


def max_size_submatrix(matrix: List[List]) -> Tuple[Tuple, Tuple]:
    n = len(matrix)
    m = len(matrix[0])

    S = [[0 for __ in range(m)] for _ in range(n)]

    g = 0
    x2, y2 = None, None
    for i in range(1, n):
        for j in range(1, m):
            if (matrix[i][j] == 1):
                left = S[i][j-1]
                top = S[i-1][j]
                top_left = S[i-1][j-1]
                state = min(left, top, top_left) + 1

                if state > g:
                    g = state
                    y2, x2 = i, j

                S[i][j] = state
            else:
                S[i][j] = 0

    x1, y1 = x2-g, y2-g

    return (x1+1, y1+1), (x2, y2)


def max_num_elements_with_largest_sum(row: List):
    def f(row: List):
        answer = l, r = 0, 0
        counter = 0
        for v in row:
            counter += v
            r += 1

            if counter >= 0:
                answer = l,r
            else:
                l = r
                counter = 0

        return answer

    a, b = f(row)

    reversed_array = reversed(row)
    c, d = f(reversed_array)
    n = len(row)
    c, d = n-d, n-c

    if b-a > d-c:
        return a, b

    return c, d


def max_sum_rectangle(matrix: List[List]) -> any:
    n = len(matrix)
    m = len(matrix[0])
    S = [[0 for __ in range(m)] for _ in range(n)]
    for i, row in enumerate(matrix):
        l, r = max_num_elements_with_largest_sum(row)
        for j in range(l, r):
            S[i][j] = 1

    (x1, y1), (x2, y2) = max_size_submatrix(S)
    answer = 0
    for i in range(y1, y2+1):
        for j in range(x1, x2+1):
            answer += matrix[i][j]

    return answer


if __name__ == '__main__':
    M = [
        [1,2,-1,-4,-20],
        [-8,-3,4,2,1],
        [3,8,10,1,3],
        [-4,-1,1,7,-6]
    ]
    print(max_sum_rectangle(M))