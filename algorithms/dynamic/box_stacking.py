from typing import List, Tuple


def max_height_stacked(boxes: List[Tuple]) -> int:
    map = {}
    for i, (l, w, _) in enumerate(boxes):
        sub = []
        for j, box in enumerate(boxes):
            x, y, _ = box
            if i != j and l >= x and w >= y:
                sub.append(j)

        map[i] = sub

    sorted_map = sorted(map.items(), key=lambda item: item[1])

    def tallest_height(k, boxes, map, answer):
        if answer[k] > 0:
            return answer[k]

        sub = [0]
        for i in map[k]:
            _ = tallest_height(i, boxes, map, answer)
            sub.append(_)

        _, _, height = boxes[k]
        answer[k] = height + max(sub)
        return answer[k]

    n = len(boxes)
    answer = [0 for _ in range(n)]
    for i, _ in sorted_map:
        tallest_height(i, boxes, map, answer)

    return max(answer)


if __name__ == '__main__':
    boxes = [
    #    l,w,h
        (1,2,2),
        (2,3,2),
        (2,4,1),
        (3,6,2),
        (1,5,4),
        (4,5,3),
    ]
    print(max_height_stacked(boxes))