from heapq import heapify, heappush as hpush, heappop as hpop


def knapsack(W, V, weight_bound):
    n = len(W)
    if n != len(V):
        raise ValueError

    R = []
    for i in range(n):
        v = V[i]
        w = W[i]
        k = w / weight_bound
        if k <= 1:
            t = v * k
            _ = -t, (v, w)
            hpush(R, _)

    value_counter = 0
    weight_counter = 0
    while R and weight_bound > weight_counter:
        _, (v, w) = hpop(R)
        value_counter += v
        weight_counter += w

    return value_counter


if __name__ == '__main__':
    W = [10, 20, 30]
    V = [60, 100, 120]
    c = 50
    print(knapsack(W, V, c))