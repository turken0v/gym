from typing import Set


# def how_many_change_can_make(X: any, S: Set, n: int = None) -> int:
#     if n is None:
#         n = len(S)

#     if X == 0:
#         return 1
    
#     if X < 0:
#         return 0

#     if n <= 0 and X >= 1:
#         return 0

#     return how_many_change_can_make(X, S, n-1) + how_many_change_can_make(X-S[n-1], S, n)


# def how_many_change_can_make(X: any, S: Set) -> int:
#     table = [1] + [0 for _ in range(X)]

#     n = len(S)
#     for i in range(n):
#         a = S[i]
#         for j in range(a, X+1):
#             table[j] += table[j-a]

#     return table[X]


def how_many_change_can_make(X: any, S: Set) -> int:
    n = len(S)

    table = [[1] * n] + [[0] * n for _ in range(X)]

    for i in range(1, X+1):
        for j in range(n):
            _ = i - S[j]
            x = table[_][j] if _ >= 0 else 0
            y = table[i][j-1] if j >= 1 else 0

            table[i][j] = x + y

    print(table)

    return table[X][-1]


if __name__ == '__main__':
    S = [1,2,3]
    print(how_many_change_can_make(4, S))