# def fib(n: int) -> int:
#     if n <= 1:
#         return n

#     return fib(n-1) + fib(n-2)

# Memoization
# def fib(n: int, lookup = {}) -> int:
#     if n <= 1:
#         lookup[n] = n

#     if lookup.get(n, None) is None:
#         lookup[n] = fib(n-1) + fib(n-2)

#     return lookup[n]

# Tabulation
def fib(n: int) -> int:
    tabulated = [0] * (n+1)
    tabulated[1] = 1
    for i in range(2, n+1):
        tabulated[i] = tabulated[i-1] + tabulated[i-2]

    return tabulated[n]

if __name__ == '__main__':
    print(fib(10))