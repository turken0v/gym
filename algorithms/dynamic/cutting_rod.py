def max_price(prices, n):
    table = [0] * (n+1)

    for i in range(1, n+1):
        value = float('-inf')
        for j in range(i):
            _ = prices[j] + table[i - j - 1]
            value = max(value, _)
        
        table[i] = value

    return table[-1]


if __name__ == '__main__':
    a = [1, 5, 8, 9, 10, 17, 17, 20]
    print(max_price(a, 8))