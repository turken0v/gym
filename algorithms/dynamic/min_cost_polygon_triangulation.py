from math import sqrt
from heapq import heapify, heappop as hpop
from itertools import cycle, tee
from typing import Dict, Set, List, Tuple


def polygon_diagonal_set(point_list: List[Tuple[any, any]]) -> Set:
    diagonal_list = set()
    n = len(point_list)
    it = cycle(point_list)
    next(it)
    for a in point_list:
        next(it)
        it, second = tee(it)
        for _ in range(n-3):
            b = next(second)
            diagonal = a, b
            reversed_points = diagonal[::-1]
            if not reversed_points in diagonal_list:
                diagonal_list.add(diagonal)

    return diagonal_list
            


def line_length(a: Tuple[any], b: Tuple[any]) -> any:
    x1, y1 = a
    x2, y2 = b

    X = (x2 - x1) ** 2
    Y = (y2 - y1) ** 2

    return sqrt(X + Y)


def lines_intersect(a: Tuple[any], b: Tuple[any]) -> bool:
    def intersection(x1a, x2a, x1b, x2b):
        return x1a <= x1b <= x2a or x1a <= x2b <= x2a

    a1, a2 = a
    x1a, y1a = a1
    x2a, y2a = a2

    if x1a > x2a:
        x1a, x2a = x2a, x1a
    
    if y1a > y2a:
        y1a, y2a = y2a, y1a

    b1, b2 = b
    x1b, y1b = b1
    x2b, y2b = b2

    if x1b > x2b:
        x1b, x2b = x2b, x1b
    
    if y1b > y2b:
        y1b, y2a = y2b, y1b

    return intersection(x1a, x2a, x1b, x2b) and intersection(y1a, y2a, y1b, y2b)


def polygon_perimeter(points: List[Tuple[any, any]]):
    n = len(points)
    counter = 0
    for i in range(n-1, -1, -1):
        a = points[i]
        b = points[i-1]
        counter += line_length(a, b)

    return counter


def min_cost_polygon_triangulation(point_list: Dict) -> any:
    answer = polygon_perimeter(point_list)

    diagonal_set = polygon_diagonal_set(point_list)
    diagonal_with_length_list = []
    for diagonal in diagonal_set:
        _ = line_length(*diagonal), diagonal
        diagonal_with_length_list.append(_)

    heapify(diagonal_with_length_list)

    # rewrite with hpop
    diagonal_count = len(diagonal_with_length_list)
    k = 0
    for i in range(diagonal_count):
        i -= k

        _, a = diagonal_with_length_list[i]
        for j in range(i):
            _, b = diagonal_with_length_list[j]

            if lines_intersect(a, b):
                diagonal_with_length_list.pop(i)
                k += 1
                break

    for diagonal_length, _ in diagonal_with_length_list:
        answer += 2 * diagonal_length

    return answer


if __name__ == '__main__':
    pentagon = [
        (0, 0),
        (0, 2),
        (1, 2),
        (2, 1),
        (1, 0),
    ]
    print(min_cost_polygon_triangulation(pentagon))