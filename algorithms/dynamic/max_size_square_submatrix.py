from typing import List


def max_size_square_submatrix(M: List[List]):
    n = len(M)
    m = len(M[0])

    S = [[0 for __ in range(m)] for _ in range(n)]

    for i in range(1, n):
        for j in range(1, m):
            if (M[i][j] == 1):
                left = S[i][j-1]
                top = S[i-1][j]
                top_left = S[i-1][j-1]
                S[i][j] = min(left, top, top_left) + 1
            else:
                S[i][j] = 0

    answer = 0
    for row in S:
        answer = max(row + [answer])

    return answer ** 2


if __name__ == '__main__':
    M = [
        [0,1,1,0,1],
        [1,1,0,1,0],
        [0,1,1,1,0],
        [1,1,1,1,0],
        [1,1,1,1,1],
        [0,0,0,0,0],
    ]
    print(max_size_square_submatrix(M))