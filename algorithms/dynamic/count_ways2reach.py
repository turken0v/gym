# https://www.geeksforgeeks.org/count-ways-reach-nth-stair/
from algorithms.dynamic.fibonacci import fib


def count_ways2reach(n: int) -> int:
    return fib(n+1)


if __name__ == '__main__':
    print(count_ways2reach(1))
    print(count_ways2reach(2))
    print(count_ways2reach(3))
    print(count_ways2reach(4))
    print(count_ways2reach(5))
    print(count_ways2reach(6))
    print(count_ways2reach(7))
