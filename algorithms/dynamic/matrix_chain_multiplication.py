from typing import List


# O(n^2)
def matrix_chain_order(p: List):
    n = len(p)
    counter = 0
    while n > 2:
        m = None
        mv = None
        for i in range(1, n-1):
            a = p[i-1]
            b = p[i+1]

            if mv > a*b or (m, mv) is (None, None):
                mv = a * b
                m = i

        counter += p[m-1] * p[m] * p[m+1]
        p.pop(m)

        n = len(p)

    return counter

if __name__ == '__main__':
    p = [40, 20, 30, 10, 30]
    print(matrix_chain_order(p))
    p = [10, 20, 30, 40, 30]
    print(matrix_chain_order(p))
    p = [10, 20, 30]
    print(matrix_chain_order(p))
