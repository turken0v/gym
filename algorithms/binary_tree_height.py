from data_structures.binary_tree import Node
from data_structures.stack import Stack


# def binary_tree_height(root: Node):
#     max_height = 0
#     first_element = root, 1
#     buffer = Stack([first_element])
#     while buffer:
#         node, level = buffer.pop()

#         if level > max_height:
#             max_height = level

#         if isinstance(node.right, Node):
#             element = node.right, level+1
#             buffer.append(element)

#         if isinstance(node.left, Node):
#             element = node.left, level+1
#             buffer.append(element)

#     return max_height


# recursive
def binary_tree_height(root: Node):
    if root is None:
        return 0

    lheight = binary_tree_height(root.left) + 1
    rheight = binary_tree_height(root.right) + 1

    return max(lheight, rheight)


if __name__ == '__main__':
    _1 = Node(1)
    _2 = Node(2)
    _3 = Node(3)
    _4 = Node(4)
    _5 = Node(5)

    _1.left = _2
    _1.right = _3
    _2.left = _4
    _2.right = _5

    print(binary_tree_height(_1))