from data_structures.binary_tree import Node
from data_structures.stack import Stack
from typing import List


def left_most(root: Node):
    node = root
    while node and isinstance(node.left, Node):
        node = node.left

    return node


def right_most(root: Node) -> Node:
    node = root
    while node and isinstance(node.right, Node):
        node = node.right

    return node


# recursive
def inorder_traversal(root: Node) -> List[any]:
    if root is None:
        return []

    answer = []

    if isinstance(root.left, Node):
        a = inorder_traversal(root.left)
        answer += a

    answer.append(root.data)

    if isinstance(root.right, Node):
        b = inorder_traversal(root.right)
        answer += b

    return answer


# def inorder_traversal(root: Node) -> List[any]:
#     answer = []
#     buffer = Stack([root])
#     explored = set()
#     while buffer:
#         node = buffer.pop()

#         if isinstance(node.left, Node) and node.left not in explored:
#             buffer.append(node)
#             buffer.append(node.left)
#             continue

#         answer.append(node.data)
#         explored.add(node)

#         if isinstance(node.right, Node) and node.right not in explored:
#             buffer.append(node.right)

#     return answer


# recursive
def preorder_traversal(root: Node) -> List[any]:
    if root is None:
        return []

    answer = []
    answer.append(root.data)

    if isinstance(root.left, Node):
        a = preorder_traversal(root.left)
        answer += a

    if isinstance(root.right, Node):
        b = preorder_traversal(root.right)
        answer += b

    return answer


# recursive
def postorder_traversal(root: Node) -> List[any]:
    if root is None:
        return []

    answer = []
    if isinstance(root.left, Node):
        a = preorder_traversal(root.left)
        answer += a

    if isinstance(root.right, Node):
        b = preorder_traversal(root.right)
        answer += b

    answer.append(root.data)

    return answer


if __name__ == '__main__':
    left = Node(2, left=Node(4), right=Node(6))
    right = Node(3, left=Node(5))
    root = Node(1, left=left, right=right)
    print(inorder_traversal(root))