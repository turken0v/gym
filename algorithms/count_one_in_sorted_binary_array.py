# https://www.geeksforgeeks.org/count-1s-sorted-binary-array/
from typing import List


def count_one_in_sorted_binary_array(elements: List) -> int:
    size = len(elements)
    l, r = 0, size-1
    while r > l:
        mid = (l + r) // 2
        if elements[mid] == 1:
            l = mid+1

            if elements[mid+1] == 0:
                return mid+1
        else:
            r = mid-1

            if elements[mid-1] == 1:
                return mid+1

    if elements[0] == 1:
        return size
    
    return 0

if __name__ == '__main__':
    a = [1, 1, 0, 0, 0, 0, 0]
    print(count_one_in_sorted_binary_array(a))
    a = [1, 1, 1, 1, 1, 1, 1]
    print(count_one_in_sorted_binary_array(a))
    a = [0, 0, 0, 0, 0, 0, 0]
    print(count_one_in_sorted_binary_array(a))
