from data_structures.stack import Stack
from typing import List


def has_cycle_directed(edges: List, first_point = 0) -> bool:
    buffer = Stack()
    directions = {}
    for u, v in edges:
        if u == first_point:
            edge = u, v, []
            buffer.append(edge)

        directions[u] = directions.get(u, []) + [v]

    explored = {
        first_point: 0
    }
    while not buffer.is_empty():
        _, v, path = buffer.pop()

        if explored.get(v, None) == 0:
            return True

        explored[v] = 0

        vertices = directions.pop(v, [])
        found = False
        for vertex in vertices:
            if explored.get(vertex, None) != 1:
                _ = v, vertex, path+[v]
                buffer.append(_)
                found = True

        if not found:
            for vertex in path:
                explored[vertex] = None

            explored[v] = 1

    return False




if __name__ == '__main__':
    # edges = [
    #     (0, 1),
    #     (0, 2),
    #     (1, 2),
    #     (2, 0),
    #     (2, 3),
    #     (3, 3),
    # ]
    # print(has_cycle_directed(edges))
    edges = [
        (0, 1),
        (1, 2),
        (2, 3),
        # (2, 0),
    ]
    print(has_cycle_directed(edges))
