from data_structures.stack import Stack
from typing import List

def partition(elements: List, low: int, high: int): 
    pivot = elements[high]
    i = low - 1
    for j in range(low, high):
        if pivot >= elements[j]:
            i += 1
            elements[i], elements[j] = elements[j], elements[i]

    elements[i+1], elements[high] = elements[high], elements[i+1]

    return i+1

def quick_sort_iterative(elements: List, low = None, high = None):
    size = len(elements)
    if low is None and high is None:
        low, high = 0, size-1

    k = high-low+1
    buffer = Stack([0]*k)

    buffer[0] = low
    buffer[1] = high

    top = 1
    while top >= 0:
        top -= 1
        high = buffer[top]
        top -= 1
        low = buffer[top]

        p = partition(elements, low, high)

        if p-1 > low:
            top += 1
            buffer[top] = low
            top += 1
            buffer[top] = p-1

        if p+1 < high:
            top += 1
            buffer[top] = p+1
            top += 1
            buffer[top] = high


if __name__ == '__main__':
    a = [4, 3, 5, 2, 1, 3, 2, 3]
    quick_sort_iterative(a)
    print(a)