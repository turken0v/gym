from typing import List


def partition(elements: List, low, high) -> None:
    pivot = elements[high]
    i = low - 1
    for j in range(low, high):
        if elements[j] < pivot:
            i += 1
            elements[i], elements[j] = elements[j], elements[i]

    elements[i+1], elements[high] = elements[high], elements[i+1]

    return i+1


def quick_sort(elements: List, low = None, high = None):
    if low is None and high is None:
        low, high = 0, len(elements)-1

    if high > low:
        pi = partition(elements, low, high)

        quick_sort(elements, low, pi-1)
        quick_sort(elements, pi+1, high)


if __name__ == '__main__':
    a = [10, 80, 30, 90, 40, 50, 70]
    quick_sort(a)
    print(a)