def shell_sort(elements): 
    n = len(elements)
    gap = n // 2

    while gap > 0:
        for i in range(gap, n):
            temp = elements[i]
            j = i
            while j >= gap and elements[j-gap] > temp:
                elements[j] = elements[j-gap]
                j -= gap

            elements[j] = temp

        gap //= 2


if __name__ == '__main__':
    a = [ 12, 34, 54, 2, 3]
    shell_sort(a)
    print(a)