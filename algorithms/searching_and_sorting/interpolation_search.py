from typing import List


def search_interpolation(array: List[any], x: any) -> int:
    size = len(array)
    l, r = 0, size-1
    while r >= l and x >= array[l] and x <= array[r]:
        i = l + round((r - l) / float(array[r] - array[l]) * (x - array[l]))

        if array[i] == x:
            return i
        elif array[i] > x:
            r = i - 1
        else:
            l = i + 1


if __name__ == '__main__':
    array = [10, 12, 13, 16, 18, 19, 20, 21, 22, 23, 24, 33, 35, 42, 47]
    print(search_interpolation(array, 36))