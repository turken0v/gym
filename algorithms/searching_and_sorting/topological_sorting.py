from data_structures.stack import Stack
from reverse_dictionary import reverse_dictionary
from typing import List, Tuple, Dict


def get_edges_count(edges: List[Tuple[any, any]]) -> Dict[any, int]:
    edges_count = {}
    for u, v in edges:
        edges_count[u] = edges_count.get(u, 0) + 1
        edges_count[v] = edges_count.get(v, 0)

    return edges_count


def get_directions(edges: List[Tuple[any, any]]) -> Dict[any, List[any]]:
    directions = {}
    for u, v in edges:
        directions[u] = directions.get(u, []) + [v]
        directions[v] = directions.get(v, [])

    return directions


def get_subvertices(edges: List[Tuple[any, any]]) -> Dict[any, List[any]]:
    directions = get_directions(edges)

    subvertices = {}
    for vertex, vertices in directions.items():
        subvertices[vertex] = set()
        for subvertex in vertices:
            subvertices[vertex].add(subvertex)
            if subvertex == vertex:
                continue

            subvertices[vertex].update(directions.get(subvertex))

    return subvertices


def _sort_topological(vertex: any, directions, answer) -> None:
    if directions.get(vertex, None) is None:
        return None

    for subvertex in directions[vertex]:
        _sort_topological(subvertex, directions, answer)

    directions[vertex] = None
    answer.append(vertex)


def sort_topological(edges: List[Tuple[any, any]]) -> List:    
    directions = get_directions(edges)
    answer = []
    for vertex in directions.keys():
        _sort_topological(vertex, directions, answer)

    return reversed(answer)


if __name__ == '__main__':
    edges = [
        (5,2),
        (5,0),
        (4,0),
        (4,1),
        (2,3),
        (3,1)
    ]
    sorted_vertices = sort_topological(edges)
    print(list(sorted_vertices))