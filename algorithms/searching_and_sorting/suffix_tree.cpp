#include <iostream>
#include <string>
using namespace std;

#define MAX_SYMBOLS 26

struct SuffixTreeNode
{
    SuffixTreeNode *childs[MAX_SYMBOLS];

    SuffixTreeNode *link = NULL;

    SuffixTreeNode *parent = NULL;

    int l, *r;

    SuffixTreeNode(int l, int *r, SuffixTreeNode *parent = NULL)
    {
        this->l = l;

        this->r = r;

        this->parent = parent;

        for (int i = 0; i < MAX_SYMBOLS; i++)
        {
            this->childs[i] = NULL;
        }
    }
};

typedef SuffixTreeNode Node;


Node* build_suffix_tree(string text)
{
    int *end = new int(0);

    Node *root = new Node(0, end),
         *node = root;

    int i = 0,
        j = 0,
        edge = -1;
    while (i < text.length())
    {
        cout << i << '-' << j << endl;
        int index = text[i] - 'a';

        if (node->childs[index] != NULL)
        {
            node = node->childs[index];
            edge = index;
            j++;
            i++;
        }
        else if (node != root)
        {
            if (text[node->l + j] == text[i])
            {
                j++;
                i++;
                continue;
            }

            while (j > 0)
            {
                auto t = node;

                node = node->parent;
                node->childs[edge] = new Node(t->l, end, node);
                node = node->childs[edge];

                t->l = node->l + j;

                node->r = new int(t->l);

                node->childs[text[t->l] - 'a'] = t;

                node->childs[text[i] - 'a'] = new Node(i, end, node);

                j--;

                if (node->link == NULL)
                {
                    int length = t->l - node->l;
                    edge = text[node->l + length - j] - 'a';
                    node->link = root->childs[edge];
                }

                node = node->link;
            }

            node = root;
            edge = -1;
        }
        else
        {
            root->childs[index] = new Node(i, end, root);
            node = root;
            edge = -1;
            i++;
        }

        (*end) = i;
    }

    return root;
}


void print_suffix_tree(Node* node, string& text, string v = "")
{
    int length = *node->r - node->l;
    v += text.substr(node->l, length);
    cout << v << endl;

    for (int i = 0; i < MAX_SYMBOLS; i++)
    {
        if (node->childs[i] != NULL)
        {
            print_suffix_tree(node->childs[i], text, v + "->");
        }
    }
}

int main()
{
    string text = "loremipsumdolor";
    Node *root = build_suffix_tree(text);

    print_suffix_tree(root, text);

    return 0;
}