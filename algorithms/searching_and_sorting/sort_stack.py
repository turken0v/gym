from data_structures.stack import Stack


def sort_stack_recursive(S: Stack) -> Stack:
    while True:
        if not S.is_empty():
            value = S.pop()
            sort_stack_recursive(S)
            if not S.is_empty() and S.top > value:
                top = S.pop()
                S += [value, top]
                continue

            S.append(value)
        break


if __name__ == '__main__':
    a = Stack([3,2,1])
    sort_stack_recursive(a)
    print(a)