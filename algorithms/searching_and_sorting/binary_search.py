from typing import List


def search_binary(array: List[any], required: any, l: int = None, r: int = None):
    l = 0 if l is None else l
    r = len(array)-1 if r is None else r
    while r >= l:
        mid = (l + r) // 2

        if array[mid] == required:
            return mid
        elif array[mid] > required:
            r = mid-1 if r == mid else mid
        else:
            l = mid+1 if l == mid else mid

if __name__ == '__main__':
    array = [2, 3, 4, 10, 40]
    print(search_binary(array, 12))