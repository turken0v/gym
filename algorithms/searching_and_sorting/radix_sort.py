from math import ceil
from typing import List


def counting4radix_sort(elements: List[any], exp: int) -> List[any]:
    size = len(elements)

    counted = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for value in elements:
        i = ceil(value / exp) % 10
        counted[i] += 1

    for i in range(1, 10):
        counted[i] += counted[i-1]

    answer = [0] * size
    for v in range(size-1, -1, -1):
        value = elements[v]
        j = ceil(value / exp) % 10
        counted[j] -= 1
        i = counted[j]
        answer[i] = value

    return answer


def radix_sort(elements: List[any]) -> List[any]:
    max_value = max(elements)

    exp = 1
    while max_value/exp > 1:
        elements = counting4radix_sort(elements, exp)
        exp *= 10

    return elements


if __name__ == '__main__':
    a = [53, 89, 150, 36, 633, 233]
    print(radix_sort(a))