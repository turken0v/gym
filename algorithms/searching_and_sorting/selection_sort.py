from typing import List


def selection_sort(A: List) -> None:
    s = len(A)
    for i in range(s):
        im = i
        for j in range(i, s):
            vm = A[im]
            v = A[j]
            if vm > v:
                im = j

        A[i], A[im] = A[im], A[i]


if __name__ == '__main__':
    A = [6,3,1,9,4,6,8,9]
    selection_sort(A)
