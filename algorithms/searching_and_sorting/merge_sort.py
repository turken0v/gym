from typing import List


def get_mid_idx(size: int):
    remainder = size % 2 if size > 1 else 0
    return size // 2 + remainder


def divide(A: List):
    size = len(A)

    assert size > 1, 'Array size must be greater or equal than 2'

    mid_idx = get_mid_idx(size)
    return A[:mid_idx], A[mid_idx:]


def merge(left: List, right: List):
    lsize, rsize = len(left), len(right)
    li, ri = 0, 0

    sorted = []
    while lsize + rsize > li + ri:

        if li >= lsize:
            sorted.append(right[ri])
            ri += 1
            continue

        if ri >= rsize:
            sorted.append(left[li])
            li += 1
            continue

        if right[ri] > left[li]:
            sorted.append(left[li])
            li += 1
        else:
            sorted.append(right[ri])
            ri += 1

    return sorted


def merge_sort(A: List):
    try:
        a, b = divide(A)
    except:
        return A

    left = merge_sort(a)
    right = merge_sort(b)

    return merge(left, right)


if __name__ == '__main__':
    A = [1,5,3,6,2,4,2,8,3,8,12,4,67,72,5,7,3,12,56,7,9,3,6,3,6,7,3]
    print(merge_sort(A))
    print(A)