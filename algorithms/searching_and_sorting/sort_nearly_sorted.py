from heapq import heapify, heappush as hpush, heappop as hpop
from typing import List


def sort_nearly_sorted(elements: List, k: int) -> None:
    buffer = elements[:k+1] # O(k)
    heapify(buffer) # O(k)

    answer = [] # O(1)
    size = len(elements) # O(1)
    # m = size - k+1
    for i in range(k+1, size): # O(m + 1)
        v = hpop(buffer) # O(log k)
        answer.append(v)  # O(1)

        u = elements[i]  # O(1)
        hpush(buffer, u)  # O(log k)
    
    # O((mlogk) + 1) => O(nlogk)

    while buffer:
        v = hpop(buffer)
        answer.append(v)

    return answer


if __name__ == '__main__':
    a = [6, 5, 3, 2, 10, 9, 8, 7]
    a = sort_nearly_sorted(a, 3)
    print(a)