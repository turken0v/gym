from algorithms.searching_and_sorting.insertion_sort import insertion_sort
from math import ceil
from typing import List


def bucket_sort(elements: List[float]) -> List[float]:
    buckets = [[] for _ in range(10)]
    for value in elements:
        i = ceil(value * 10)
        buckets[i].append(value)

    i = 0
    for bucket in buckets:
        insertion_sort(bucket)

        for value in bucket:   
            elements[i] = value
            i += 1


if __name__ == '__main__':
    a = [0.897, 0.565, 0.656, 0.1234, 0.665, 0.3434]
    bucket_sort(a)
    print(a)