from typing import List


def search_linear(array: List, required: any) -> int:
    for i, value in enumerate(array):
        if value == required:
            return i


if __name__ == '__main__':
    array = [10, 20, 80, 30, 60, 50, 110, 100, 130, 170]
    print(search_linear(array, 110))