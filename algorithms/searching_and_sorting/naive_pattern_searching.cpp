#include <iostream>
using namespace std;


int naive_searching(string pattern, string text)
{
    int A = pattern.length(), B = text.length();
    for (int i = 0, j; i < B; i++)
    {
        j = 0;
        while (j < A && text[i+j] == pattern[j])
        {
            j++;
        }

        if (j == A)
        {
            return i;
        }
    }

    return -1;
}


int main()
{
    string text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

    cout << naive_searching("survived", text) << endl;

    return 0;
}