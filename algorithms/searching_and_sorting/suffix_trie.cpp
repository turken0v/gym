#include <iostream>
#include <string>
#include <list>
using namespace std;


#define MAX_SYMBOLS 26

struct SuffixTrieNode
{
    SuffixTrieNode *childs[MAX_SYMBOLS];
    list<int> *indexes;

    SuffixTrieNode()
    {
        this->indexes = new list<int>;

        for (int i = 0; i < MAX_SYMBOLS; i++)
        {
            this->childs[i] = NULL;
        }
    }

    void insert(string input, int i);
};


void SuffixTrieNode::insert(string input, int i)
{
    this->indexes->push_back(i);

    if (input.length() == 0)
    {
        return ;
    }

    int index = input[0] - 'a';

    if (this->childs[index] == NULL)
    {
        this->childs[index] = new SuffixTrieNode();
    }

    this->childs[index]->insert(input.substr(1), i+1);
}


SuffixTrieNode * build_suffix_trie(string input)
{
    SuffixTrieNode *root = new SuffixTrieNode();
    for (int i = 0; i < input.length(); i++)
    {
        root->insert(input.substr(i), i);
    }

    return root;
}


void print_suffix_trie(SuffixTrieNode *node, string v = "")
{
    cout << v << endl;

    char symbol;
    for (int i = 0; i < MAX_SYMBOLS; i++)
    {
        if (node->childs[i] != NULL)
        {
            symbol = 'a' + i;
            print_suffix_trie(node->childs[i], v + symbol);
        }
    }
}


int main()
{
    auto root = build_suffix_trie("abcabcbb");
    print_suffix_trie(root);

    return 0;
}