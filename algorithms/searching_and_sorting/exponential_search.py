from searching_and_sorting.binary_search import search_binary
from typing import List


def search_exponential(array: List[any], x: any) -> int:
    size = len(array)

    if array[0] == x:
        return 0

    i = 1
    while size > i and x >= array[i]:
        i *= 2

    r = int(min(i, size))
    l = int(i/2)
    return search_binary(array, x, l, r)


if __name__ == '__main__':
    array = [10, 20, 40, 45, 55]
    print(search_exponential(array, 45))