from typing import List


def next_gap(gap: int):
    return int(gap / 1.3)


def comb_sort(elements: List[any]):
    size = len(elements)
    gap = size
    swapped = True
    while gap > 1 or swapped:
        swapped = False
        gap = next_gap(gap)
        for i in range(0, size-gap):
            if elements[i] > elements[i+gap]:
                elements[i], elements[i+gap] = elements[i+gap], elements[i]
                swapped = True


if __name__ == '__main__':
    a = [8, 4, 1, 3, -44, 23, -6, 28, 0]
    comb_sort(a)
    print(a)