from typing import List

def bubble_sort(A: List[int]) -> None:
    size = len(A)
    unsorted = True
    while unsorted:
        unsorted = False

        for i, value in enumerate(A):
            j = i + 1
            if j >= size:
                break
            next_value = A[j]

            if value > next_value:
                A[i], A[j] = A[j], A[i]
                unsorted = True


if __name__ == '__main__':
    a = [3,2,1]
    bubble_sort(a)
    print(a)