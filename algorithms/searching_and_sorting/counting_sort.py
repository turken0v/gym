from typing import List


def sort_counting(elements: List[any]) -> List[any]:
    size = len(elements)
    max_element = max(elements)
    min_element = min(elements)
    range_of_elements = max_element - min_element + 1

    counted = [0 for _ in range(range_of_elements)]
    for i in range(0, size):
        idx = elements[i] - min_element
        counted[idx] += 1

    for i in range(1, range_of_elements):
        counted[i] += counted[i-1]

    answer = [0 for _ in range(size)]
    for i in range(size-1, -1, -1):
        j = elements[i] - min_element
        counted[j] -= 1
        idx = counted[j]
        answer[idx] = elements[i]
 
    return answer


if __name__ == '__main__':
    a = [1, 4, 1, 2, 7, 5, 2]
    print(sort_counting(a))