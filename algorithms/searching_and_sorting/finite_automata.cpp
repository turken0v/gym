#include <bits/stdc++.h>
using namespace std;
#define MAX_SYMBOLS 26


void transition_function(char* pattern, int M, int table[][MAX_SYMBOLS])
{
	for (int i = 0; i < MAX_SYMBOLS; i++)
	{
		table[0][i] = 0;
	}
	table[0][pattern[0] - 'A'] = 1;

	for (int i = 1, lps = 0; i <= M; i++)
	{
		for (int j = 0; j < MAX_SYMBOLS; j++)
		{
			table[i][j] = table[lps][j];
		}

		int index = pattern[i] - 'A';
		table[i][index] = i + 1;

		if (i < M)
		{
			lps = table[lps][index];
		}
	}
}

void search(char pattern[], char text[])
{
	int M = strlen(pattern);
	int N = strlen(text);

	int table[M + 1][MAX_SYMBOLS];

	transition_function(pattern, M, table);

	// process text over FA.
	for (int i = 0, j = 0; i < N; i++) {
		int index = text[i] - 'A';
		j = table[j][index];
		
		if (j == M) {
			cout << "pattern found at index " << i - M + 1 << endl;
		}
	}
}

int main()
{
	// char txt[] = "ABCABCBB";
	// char pat[] = "ABC";
	// search(pat, txt);

	char text[] = "ABCABCBB";

	int M = strlen(text);

	int table[M + 1][MAX_SYMBOLS];

	transition_function(text, M, table);

	for (int i = 0; i < MAX_SYMBOLS; i++)
	{
		cout << char ('A' + i) << ' ';
	}

	cout << endl;

	for (int i = 0; i < M+1; i++)
	{
		for (int j = 0; j < MAX_SYMBOLS; j++)
		{
			cout << table[i][j] << ' ';
		}

		cout << endl;
	}

	return 0;
}