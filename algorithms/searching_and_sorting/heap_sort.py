from data_structures.deque import Deque
from data_structures.heap import hpop
from typing import List


def heap_sort(elements: List) -> None:
    sorted = Deque()
    while elements:
        value = hpop(elements)
        sorted.appendleft(value)

    return list(sorted)


if __name__ == '__main__':
    array = [20,15,9,6,11,8,5,4,3,7]
    print(heap_sort(array))