from typing import List


def insertion_sort(array: List[any]) -> None:
    size = len(array)
    for i in range(1, size):
        if array[i] >= array[i-1]:
            continue

        j = i - 1
        while j > 0 and array[j] > array[i]:
            j -= 1

        current = array.pop(i)
        array.insert(j, current)

if __name__ == '__main__':
    array = [4,3,2,10,12,1,5,6]
    insertion_sort(array)
    print(array)