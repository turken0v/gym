from typing import List


def pigeonhole_sort(elements: List[int]) -> None:
    a = min(elements)
    z = max(elements)
    size = z - a + 1
    holes = [0] * size
    for value in elements:
        assert type(value) is int, ValueError('Integer only')
        holes[value-a] += 1

    i = 0
    for count in range(size):
        while holes[count] > 0:
            holes[count] -= 1
            elements[i] = count + a
            i += 1


if __name__ == '__main__':
    a = [8, 3, 2, 7, 4, 6, 8]
    pigeonhole_sort(a)
    print(a)