from math import sqrt, floor
from typing import List


def search_jump(array: List[any], required: any) -> int:
    size = len(array)
    m = floor(sqrt(size))
    l = 0
    for i in range(1, m):
        i *= m
        if array[i] > required:
            break

        l = i

    for i in range(m):
        i += l
        if array[i] == required:
            return i


if __name__ == '__main__':
    array = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610]
    print(search_jump(array, 144))