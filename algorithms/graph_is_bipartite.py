from typing import List, Tuple
from data_structures.deque import Deque


def is_bipartite(edges: List[Tuple], fr0m = 0) -> Tuple[List, List]:
    directions = {}
    for a, b in edges:
        directions[a] = directions.get(a, []) + [b]
        directions[b] = directions.get(b, [])

    red = set()
    blue = set()
    qelement = fr0m, 0
    buffer = Deque([qelement])
    while buffer:
        vertex, level = buffer.popleft()

        if level % 2 == 0:
            red.add(vertex)
        else:
            blue.add(vertex)

        vertices = directions.pop(vertex, None)
        if vertices:
            buffer += [(vertex, level+1) for vertex in vertices]

    return not len(red & blue) > 0


if __name__ == '__main__':
    edges = [
        (0, 1),
        (0, 2),
        (1, 3),
        (2, 4),
        # (3, 4),
        (3, 5),
        (4, 5)
    ]
    print(is_bipartite(edges))