# def factorial(n: int) -> int:
#     if n <= 1:
#         return 1

#     return n * factorial(n-1)


def factorial(n: int, memo: dict = {}) -> int:
    if n <= 1:
        return 1

    value = memo.get(n)
    if value:
        return value

    value = n * factorial(n-1, memo)

    return value

if __name__ == '__main__':
    print(factorial(4))