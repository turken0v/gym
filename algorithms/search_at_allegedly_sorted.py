# https://www.geeksforgeeks.org/search-almost-sorted-array/
from typing import List


def search_at_allegedly_sorted(elements: List, x, l: int = None, r: int = None) -> int:
    if l is None and r is None:
        size = len(elements)
        l, r = 0, size-1

    while r > l:
        mid = (l + r) // 2

        for i in range(mid-1, mid+2):
            if elements[i] == x:
                return mid

        if elements[i] > x:
            r = mid-2
        else:
            l = mid+2



if __name__ == '__main__':
    a = [10, 3, 40, 20, 50, 80, 70]
    print(search_at_allegedly_sorted(a, 40))