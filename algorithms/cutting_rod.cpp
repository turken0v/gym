#include <bits/stdc++.h>
using namespace std;


int cut_rod(vector<int> &P)
{
    int n = P.size() + 1;

    int T[n];
    memset(T, 0, sizeof(T));

    for (int i = 1; i < n; i++)
    {
        for (int j = 1; j < n; j++)
        {
            if (i - j >= 0)
            {
                T[i] = max(T[i], P[j-1] + T[i - j]);
            }
        }
    }

    return T[n-1];
}

int main()
{
    vector<int> input;

    input = {1,5,8,9,10,17,17,20};
    cout << cut_rod(input) << endl << endl;

    input = {3,5,8,9,10,17,17,20};
    cout << cut_rod(input) << endl;

    return 0;
}