from heapq import heappop as hpop, heappush as hpush


def dijkstra(V, E, W):
    _V = list(V)
    start = _V[0]
    end = _V[-1]
    explored = set()
    Q = []
    hpush(Q, (0, [start]))

    n = len(E)
    if n != len(W):
        raise ValueError

    while len(Q):
        wsum, path = hpop(Q)
        vertex = path[-1]
        explored.add(vertex)

        for i in range(n):
            edge = E[i]
            a = edge[0]
            b = edge[1]
            weight = 1 if W[i] is None else W[i]
            if a == vertex and b not in explored:
                p = path + [b]
                w = wsum + weight

                if b == end:
                    return p, w

                hpush(Q, (w, p))


if __name__ == '__main__':
    # M = (
    #     0,  10, 5,  0,  0,
    #     10,  0, 0,  8,  0,
    #     5,   0, 0,  8,  0,
    #     0,   8, 8,  0, 15,
    #     0,   0, 0, 15,  0
    # )

    V = set([1, 2, 3, 4, 5])
    E = (
        (1, 2), (1, 3),
        (2, 4),
        (3, 4),
        (4, 5),
    )
    # W = (
    #     10, 5,
    #     8,
    #     14,
    #     15
    # )