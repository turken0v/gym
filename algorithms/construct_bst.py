from data_structures.binary_tree import Node, print_inorder_binary_tree
from typing import List

def _construct_bst(root: Node, left: List, right: List) -> Node:
    if left:
        size = len(left)-1
        mid = size // 2
        value = left.pop(mid)
        root.left = Node(value)
        _construct_bst(root.left, left[:mid], left[mid:])

    if right:
        size = len(right)-1
        mid = size // 2
        value = right.pop(mid)
        root.right = Node(value)
        _construct_bst(root.right, right[:mid], right[mid:])


def construct_bst(elements: List) -> Node:
    size = len(elements)
    mid = size // 2

    value = elements.pop(mid)
    root = Node(value)
    _construct_bst(root, elements[:mid], elements[mid:])

    return root


if __name__ == '__main__':
    root = construct_bst([1,2,3,4,5,6,7,8,9,10,11])
    print_inorder_binary_tree(root)