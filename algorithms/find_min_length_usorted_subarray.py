from data_structures.stack import Stack
from typing import List, Tuple


def find_min_length_unsorted_subarray(elements: List) -> Tuple[int, int]:
    _ = elements[0], 0
    buffer = Stack([ _, ])
    l, r, max_value = None, None, elements[0]
    size = len(elements)
    for i in range(1, size):
        value = elements[i]

        top_value, top_index = buffer.pop()
        if top_value > value:
            if top_value > max_value:
                max_value = top_value

            if l is None:
                l = top_index

            r = None

        if r is None and value > max_value:
            r = i-1

        _ = top_value, top_index
        buffer.append(_)

        _ = value, i
        buffer.append(_)

    l = 0 if l is None else l
    r = size-1 if r is None else r

    return l, r


if __name__ == '__main__':
    a = [10, 12, 20, 30, 25, 40, 32, 31, 35, 50, 60]
    print(find_min_length_unsorted_subarray(a))