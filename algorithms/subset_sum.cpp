#include <bits/stdc++.h>
using namespace std;


bool has_subset_with_sum(vector<int> &nums, int x) {
    int n = nums.size();

    bool A[x+1][n];
    memset(A, false, sizeof(bool)*(x+1)*n);

    bool B[x+1];
    memset(B, false, sizeof(bool)*(x+1));
    B[0] = true;

    for (int i = 1; i <= x; i++) {
        for (int j = 0; j < n; j++) {
            if (nums[j] > i || !B[i-nums[j]] || A[i-nums[j]][j]) {
                continue;
            }

            for (int k = 0; k < n; k++) {
                A[i][k] = A[i-nums[j]][k];
            }

            A[i][j] = true;
            B[i] = true;

            break;
        }
    }

    return B[x];
}


int main() {
    vector<int> input;

    input = {3, 34, 4, 12, 5, 2};
    cout << has_subset_with_sum(input, 9) << endl;

    input = {3, 34, 4, 12, 5, 2};
    cout << has_subset_with_sum(input, 30) << endl;

    return 0;
}