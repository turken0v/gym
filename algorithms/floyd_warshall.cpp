#include <bits/stdc++.h>
using namespace std;

typedef vector<vector<int>> vector_square;

void shortest_distance(vector_square &graph)
{
    int n = graph.size();

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (i == j || graph[i][j] == INT_MAX)
            {
                continue;
            }

            for (int k = 0; k < i; k++)
            {
                graph[k][j] = min(graph[k][j], graph[i][j] + graph[k][j-1]);
            }
        }
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            if (graph[i][j] > 1000)
            {
                cout << "~ ";
            }
            else
            {
                cout << graph[i][j] << ' ';
            }
        }

        cout << endl;
    }
}


int main()
{
    vector_square graph {
        {0, 5, INT_MAX, 10},
        {INT_MAX, 0, 3, INT_MAX},
        {INT_MAX, INT_MAX, 0, 1},
        {INT_MAX, INT_MAX, INT_MAX, 0}
    };

    shortest_distance(graph);

    return 0;
}