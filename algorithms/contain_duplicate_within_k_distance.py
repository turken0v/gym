from typing import List


def contain_duplicate_within_k_distance(elements: List, k: int) -> bool:
    size = len(elements)
    assert k > 0 and size > k, ValueError

    buffer = {}
    for i, value in enumerate(elements[:k]):
        buffer[value] = buffer.get(value, []) + [i]

    for i, value in enumerate(elements):
        duplicates = buffer[value]
        for index in duplicates:
            if i == index:
                continue

            if index >= i-k and i+k >= index:
                return True

        index = i+k
        if size > index:
            value = elements[index]
            buffer[value] = buffer.get(value, []) + [index]

    return False


if __name__ == '__main__':
    print(contain_duplicate_within_k_distance([1, 2, 3, 4, 1, 2, 3, 4], 3))