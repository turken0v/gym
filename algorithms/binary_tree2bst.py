from data_structures.binary_tree import Node, print_inorder_binary_tree
from typing import List
from .merge_sort import merge_sort
from construct_bst import construct_bst


def binary_tree2list_with_data(root: Node, _buffer = []) -> List:
    _buffer.append(root.data)

    if isinstance(root.left, Node):
        binary_tree2list_with_data(root.left, _buffer)

    if isinstance(root.right, Node):
        binary_tree2list_with_data(root.right, _buffer)

    return _buffer

def binary_tree2bst(root: Node):
    array = binary_tree2list_with_data(root)
    array = merge_sort(array)
    return construct_bst(array)


if __name__ == '__main__':
    _10 = Node(10)
    _2 = Node(2)
    _7 = Node(7)
    _8 = Node(8)
    _4 = Node(4)

    _10.left = _2
    _10.right = _7
    _2.left = _8
    _2.right = _4

    root = binary_tree2bst(_10)
    print_inorder_binary_tree(root)