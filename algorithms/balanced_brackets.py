from data_structures.stack import Stack

def brackets_is_balanced(expression: str) -> bool:
    brackets = Stack()
    for char in expression:
        if char in ('(', '[', '{'):
            brackets.append(char)
        elif char == ')':
            if brackets.pop() != '(':
                return False
        elif char == ']':
            if brackets.pop() != '[':
                return False
        elif char == '}':
            if brackets.pop() != '{':
                return False

    return True


if __name__ == '__main__':
    print(brackets_is_balanced('(()[{}])'))