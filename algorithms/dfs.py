from data_structures.stack import Stack


def DFS(V, E):
    _V = list(V)
    start = _V[0]
    end = _V[-1]
    explored = set()
    S = Stack([[start]])

    while not S.empty:
        path = S.pop()
        vertex = path[-1]
        explored.add(vertex)

        for edge in E:
            a = edge[0]
            b = edge[1]
            if a == vertex and b not in explored:
                p = path + [b]

                if b == end:
                    return p

                S.push(p)


if __name__ == '__main__':
    # M = (
    #     0,  10, 5,  0,  0,
    #     10,  0, 0,  8,  0,
    #     5,   0, 0,  8,  0,
    #     0,   8, 8,  0, 15,
    #     0,   0, 0, 15,  0
    # )

    V = set([1, 2, 3, 4, 5])
    E = (
        (1, 2), (1, 3),
        (2, 4),
        (3, 4),
        (4, 5),
    )
    # W = (
    #     10, 5,
    #     8,
    #     14,
    #     15
    # )