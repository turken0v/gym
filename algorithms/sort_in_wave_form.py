from typing import List


# def sort_in_wave(elements: List) -> List:
#     sorted_elements = sorted(elements)
#     answer = []
#     while sorted_elements:
#         try:
#             left = sorted_elements.pop(0)
#             answer.append(left)
#             right = sorted_elements.pop()
#             answer.append(right)
#         except:
#             pass

#     return answer


def sort_in_wave(elements: List) -> None:
    size = len(elements)
    for i in range(0, size, 2):
        if i > 1 and elements[i] > elements[i-1]:
            elements[i], elements[i-1] = elements[i-1], elements[i]

        if size-1 > i and elements[i] > elements[i+1]:
            elements[i], elements[i+1] = elements[i+1], elements[i]


if __name__ == '__main__':
    a = [10, 5, 6, 3, 2, 20, 100, 80]
    sort_in_wave(a)
    print(a)