#include <bits/stdc++.h>
using namespace std;


int sum(vector<int> &nums)
{
    int counter = 0;
    for (int i = 0; i < nums.size(); i++)
    {
        counter += nums[i];
    }

    return counter;
}


// bool has_two_subsets(vector<int> &nums)
// {
//     int n = nums.size(),
//         l = sum(nums) + 1;

//     bool A[l], B[l][n];
//     memset(A, false, sizeof(bool)*l);
//     memset(B, false, sizeof(bool)*l*n);
//     A[0] = true;

//     vector<int> C;
//     for (int i = 1; i < l; i++)
//     {
//         C = {};
//         for (int j = n-1; j > -1; j--)
//         {
//             if (nums[j] > i || !A[i-nums[j]] || B[i-nums[j]][j])
//             {
//                 continue;
//             }

//             int bitset = 0;
//             for (int k = 0; k < n; k++)
//             {
//                 B[i][k] = B[i-nums[j]][k];
//                 if (k == j)
//                 {
//                     B[i][k] = true;
//                 }

//                 if (B[i][k])
//                 {
//                     bitset += 1;
//                 }

//                 bitset <<= 1;
//             }

//             C.push_back(bitset);

//             A[i] = true;
//         }

//         for (int j = 0; j < C.size(); j++)
//         {
//             for (int k = 0; k < C.size(); k++)
//             {
//                 if (j == k)
//                 {
//                     continue;
//                 }

//                 if ((C[j] & C[k]) == 0)
//                 {
//                     return true;
//                 }
//             }
//         }
//     }

//     return false;
// }

#define MAX_SET_SIZE 4096

bool has_two_subsets(vector<int> &nums)
{
    int n = nums.size(),
        l = sum(nums) + 1;

    bool A[l], B[l][n];
    memset(A, false, sizeof(bool)*l);
    memset(B, false, sizeof(bool)*l*n);
    A[0] = true;

    vector<bitset<MAX_SET_SIZE>> C;
    for (int i = 1; i < l; i++)
    {
        C = {};
        for (int j = n-1; j > -1; j--)
        {
            if (nums[j] > i || !A[i-nums[j]] || B[i-nums[j]][j])
            {
                continue;
            }

            bitset<MAX_SET_SIZE> bset;
            for (int k = 0; k < n; k++)
            {
                B[i][k] = B[i-nums[j]][k];
                if (k == j)
                {
                    B[i][k] = true;
                }

                bset[k] = B[i][k];
            }

            C.push_back(bset);

            A[i] = true;
        }

        for (int j = 0; j < C.size(); j++)
        {
            for (int k = 0; k < C.size(); k++)
            {
                if (j == k)
                {
                    continue;
                }

                if ((C[j] & C[k]) == 0)
                {
                    return true;
                }
            }
        }
    }

    return false;
}


int main()
{
    vector<int> input;

    input = {1,5,10,5};
    cout << has_two_subsets(input) << endl;

    input = {1,2,3,4};
    cout << has_two_subsets(input) << endl;

    input = {1,2,3};
    cout << has_two_subsets(input) << endl;

    input = {1,2,4};
    cout << has_two_subsets(input) << endl;

    return 0;
}