from reverse_dictionary import reverse_dictionary
from typing import Dict


def find_all_employees_under_each_manager(dataset: Dict) -> Dict:
    answer = {}
    managers = reverse_dictionary(dataset)
    for manager, employees in managers.items():
        num = 0
        for employee in employees:
            found = answer.get(employee, 0)
            answer[employee] = found

            num += found + 1

        answer[manager] = num

    return answer

if __name__ == '__main__':
    employees = {
        "A": "C",
        "B": "C",
        "C": "F",
        "D": "E",
        "E": "F",
        "F": "F"
    }
    print(find_all_employees_under_each_manager(employees))