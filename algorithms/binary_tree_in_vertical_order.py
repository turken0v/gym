from data_structures.binary_tree import Node
from typing import Dict


def get_nodes_horizontal_position(root: Node, x = 0, nodes = {}) -> Dict[int, Node]:
    if root is None:
        return nodes

    nodes[x] = nodes.get(x, []) + [root]

    if isinstance(root.left, Node):
        get_nodes_horizontal_position(root.left, x-1, nodes)

    if isinstance(root.right, Node):
        get_nodes_horizontal_position(root.right, x+1, nodes)

    return nodes

def print_in_vertical_order(root: Node):
    nodes = get_nodes_horizontal_position(root)
    keys = nodes.keys()
    for key in sorted(keys):
        print(nodes[key])


if __name__ == '__main__':
    _1 = Node(1)
    _2 = Node(2)
    _3 = Node(3)
    _4 = Node(4)
    _5 = Node(5)
    _6 = Node(6)
    _7 = Node(7)
    _8 = Node(8)
    _9 = Node(9)

    _1.left = _2
    _1.right = _3
    _2.left = _4
    _2.right = _5
    _3.left = _6
    _3.right = _7
    _7.left = _8
    _7.right = _9

    print_in_vertical_order(_1)