from data_structures.binary_tree import Node
from data_structures.deque import Deque


def are_identical(first: Node, second: Node) -> bool:
    if first != second:
        return False

    if isinstance(first.left, Node):
        if not are_identical(first.left, second.left):
            return False

    if isinstance(first.right, Node):
        if not are_identical(first.right, second.right):
            return False

    return True

def is_subtree(first: Node, second: Node) -> bool:
    deq = Deque([first])
    while not deq.is_empty():
        node = deq.popleft()

        if node == second:
            if are_identical(second, node):
                return True

        if isinstance(node.right, Node):
            deq.append(node.right)

        if isinstance(node.left, Node):
            deq.append(node.left)

    return False


if __name__ == '__main__':
    _11 = Node(1)
    _12 = Node(2)
    _13 = Node(3)
    _14 = Node(4)
    _15 = Node(5)
    _16 = Node(6)
    _17 = Node(7)

    _11.left = _12
    _11.right = _13
    _12.left = _14
    _12.right = _15
    _14.right = _16
    _13.right = _17

    _21 = Node(2)
    _22 = Node(4)
    _23 = Node(5)
    _24 = Node(6)

    _21.left = _22
    _21.right = _23
    _22.right = _24

    print(is_subtree(_11, _21))