from data_structures.heap import heapify, hpop
from typing import List


def get_largest(elements: List, k: int) -> List:
    assert len(elements) >= k, ValueError

    answer = []
    heapify(elements)
    for _ in range(k):
        largest = hpop(elements)
        answer.append(largest)

    return answer


if __name__ == '__main__':
    print(get_largest([0,1,2,3,4,5,6,7,8], 3))