# count_parts - Count number of partitions of a set with n elements into k subsets
# Returns count of different partitions of n elements in k subsets

# def count_parts(n: int, k: int) -> int:
#     if n == 0 or k == 0 or k > n:
#         return 0
#     if k == 1 or k == n:
#         return 1

#     return k * count_parts(n-1, k) + count_parts(n-1, k-1)


def count_parts(n: int, k: int) -> int:
    M = [[0 for i in range(k + 1)] for j in range(n + 1)]

    for i in range(n + 1):
        M[i][0] = 0

    for i in range(k + 1):
        M[0][k] = 0

    for i in range(1, n + 1):
        for j in range(1, k + 1):
            if (j == 1 or i == j):
                M[i][j] = 1
            else:
                M[i][j] = (j * M[i - 1][j] + M[i - 1][j - 1])

    return M[n][k]


if __name__ == '__main__':
    print(count_parts(10, 9))