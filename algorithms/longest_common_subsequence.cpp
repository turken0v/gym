#include <bits/stdc++.h>
using namespace std;


// int LCS(string A, string B) {
//     int n = A.length(), m = B.length();

//     if (n <= 0 || m <= 0) {
//         return 0;
//     }

//     string a = A.substr(0, n-1), b = B.substr(0, m-1);

//     if (A[n-1] == B[m-1]) {
//         return 1 + LCS(a, b);
//     }

//     return max(LCS(A, b), LCS(a, B));
// }


int LCS(string A, string B) {
    int n = A.length(), m = B.length();

    int T[m+1][n+1];
    memset(T, 0, sizeof(int)*(m+1)*(n+1));

    for (int i = 1; i < m+1; i++) {
        for (int j = 1; j < n+1; j++) {
            if (B[i-1] == A[j-1]) {
                T[i][j] = T[i-1][j] + 1;
            } else {
                T[i][j] = max(T[i-1][j], T[i][j-1]);
            }
        }
    }

    return T[m][n];
}


int main() {
    cout << "Length of longest common subsequence: " << LCS("AGGTAB", "GXTXAYB") << endl;

    return 0;
}