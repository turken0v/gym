from typing import List


def count_sort(elements: List, n: int, exp: int) -> List:
    counted = [0] * n
    for value in elements:
        i = (value // exp) % n
        counted[i] += 1

    for i in range(1, n):
        counted[i] += counted[i-1]

    answer = [0] * n
    for i in range(n-1, -1, -1):
        value = elements[i]
        j = (value // exp) % n
        counted[j] -= 1
        index = counted[j]
        answer[index] = value

    return answer


def sort_linear(elements: List):
    n = len(elements)
    answer = count_sort(elements, n, 1)
    answer = count_sort(elements, n, n)
    return answer


if __name__ == '__main__':
    a = [0, 23, 14, 12, 9]
    print(sort_linear(a))