from data_structures.binary_tree import Node, print_inorder_binary_tree


def clone_binary_tree(root: Node) -> Node:
    node = Node()

    if isinstance(root.left, Node):
        node.left = clone_binary_tree(root.left)

    if isinstance(root.right, Node):
        node.right = clone_binary_tree(root.right)

    node.data = root.data

    return node


if __name__ == '__main__':
    _1 = Node(1)
    _2 = Node(2)
    _3 = Node(3)
    _4 = Node(4)
    _5 = Node(5)

    _1.left = _2
    _1.right = _3
    _2.left = _4
    _2.right = _5

    cloned = clone_binary_tree(_1)
    print_inorder_binary_tree(cloned)