from data_structures.binary_tree import Node
from depth_first_traversal import inorder_traversal

# # O(n)
# # S(n)
# def find_kth_smallest(root: Node, k: int) -> Node:
#     nodes = inorder_traversal(root)
#     size = len(nodes)
#     return None if k > size else nodes[k-1]


def _kth_smallest(root: Node, k: int) -> Node:
    if isinstance(root.left, Node):
        _kth_smallest(root.left, k)
    
    _kth_smallest.counter += 1

    if _kth_smallest.counter >= k:
        if _kth_smallest.answer is None:
            _kth_smallest.answer = root

        return _kth_smallest.answer

    if isinstance(root.right, Node):
        _kth_smallest(root.right, k)


def find_kth_smallest(root: Node, k: int) -> Node:
    setattr(_kth_smallest, 'counter', 0)
    setattr(_kth_smallest, 'answer', None)
    return _kth_smallest(root, k)


if __name__ == '__main__':
    _20 = Node(20)
    _8 = Node(8)
    _22 = Node(22)
    _4 = Node(4)
    _12 = Node(12)
    _10 = Node(10)
    _14 = Node(14)

    _20.left = _8
    _20.right = _22
    _8.left = _4
    _8.right = _12
    _12.left = _10
    _12.right = _14

    print(find_kth_smallest(_20, 3))