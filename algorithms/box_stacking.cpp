#include <bits/stdc++.h>
using namespace std;


struct Box {
    unsigned int w, h, d;
    Box(unsigned int w, unsigned int h, unsigned int d) {
        this->w = w;
        this->h = h;
        this->d = d;
    }
};


int max_height_stacked_boxes(vector<Box *> &boxes) {
    int n = 33;

    int M[n][n];
    memset(M, 0, sizeof(int)*n*n);

    Box *box;
    for (int i = 0; i < boxes.size(); i++) {
        box = boxes[i];

        M[box->w][box->h] += box->d;
        M[box->h][box->w] += box->d;
        M[box->w][box->d] += box->h;
        M[box->d][box->w] += box->h;
        M[box->h][box->d] += box->w;
        M[box->d][box->h] += box->w;
    }

    for (int i = 1; i < n; i++) {
        for (int j = 1; j <= i; j++) {
            cout << M[i][j] << ' ';
        }

        cout << endl;
    }

    int a, b;
    for (int i = 1; i < n; i++) {
        for (int j = 1; j <= i; j++) {
            a = M[i][j] + M[i-1][j];
            b = M[i][j] + M[i][j-1];
            if (M[i][j] > 0)
            {
                if (i > M[i][j]) {
                    a -= i;
                }
                if (j > M[i][j]) {
                    b -= j;
                }
            }

            if (a > b) {
                M[i][j] = a;
            } else {
                M[i][j] = b;
            }
        }
    }

    for (int i = 1; i < n; i++) {
        for (int j = 1; j <= i; j++) {
            cout << M[i][j] << ' ';
        }

        cout << endl;
    }

    return M[n-1][n-1];
}


int main() {
    vector<Box *> input;
    input = {
        new Box( 4,  6,  7),
        new Box( 1,  2,  3),
        new Box( 4,  5,  6),
        new Box(10, 12, 32),
    };
    // input = {
    //     new Box( 2,  2,  3),
    //     new Box( 8,  4,  2),
    //     new Box(16,  8,  1),
    // };
    cout << max_height_stacked_boxes(input) << endl;

    return 0;
}