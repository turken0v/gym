#include <bits/stdc++.h>
using namespace std;


int LIS(vector<int> input)
{
    int n = input.size();

    int T[n];
    for (int i = 0; i < n; i++)
    {
        T[i] = 1;
    }

    int length = 1;
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < i; j++)
        {
            if (input[i] > input[j])
            {
                T[i] = max(T[i], T[j]+1);
            }
        }

        length = max(length, T[i]);
    }

    return length;
}


int main()
{
    vector<int> input {3, 10, 2, 1, 20};
    cout << "Longest increasing subsequence length: " << LIS(input) << endl;

    return 0;
}