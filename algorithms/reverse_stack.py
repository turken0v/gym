from data_structures.stack import Stack

def reverse_stack_recursive(S: Stack) -> Stack:
    N = Stack()
    if not S.is_empty():
        element = S.pop()
        R = reverse_stack_recursive(S)
        N.append(element)
        N += R

    return N


if __name__ == '__main__':
    a = Stack([1,2,3])
    print(reverse_stack_recursive(a))