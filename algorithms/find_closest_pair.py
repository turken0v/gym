from algorithms.arg_min import arg_min
from algorithms.searching_and_sorting.merge_sort import merge
from typing import List, Tuple


# def closest_binary_search(elements: List, required) -> any:
#     size = len(elements)
#     l, r = 0, size-1
#     mid = 0
#     while r > l:
#         mid = (l + r) // 2
#         if elements[mid] == required:
#             return elements[mid]
#         elif elements[mid] > required:
#             r = mid-1
#         else:
#             l = mid+1

#     deltas = [(i, abs(required - elements[i])) for i in range(mid-1, mid+2) if size > i and i >= 0]
#     i, _ = arg_min([value for _, value in deltas])
#     closest = deltas[i][0]

#     return elements[closest]


# def find_closest_pair(a: List, b: List, required) -> Tuple[any, any]:
#     pairs = [(value, closest_binary_search(b, abs(required - value))) for value in a]
#     i, _ = arg_min([abs(a + b - required) for a, b in pairs])
#     return pairs[i]


def find_closest_pair(a: List, b: List, required) -> Tuple[any, any]:
    elements = merge(a, b)
    i = 0
    j = len(elements)-1
    delta, last_delta, pair = elements[j] + elements[i] - required, None, None
    while last_delta is None or j > i and abs(last_delta) > abs(delta):
        pair = elements[i], elements[j]
        last_delta = delta
        if delta > 0:
            j -= 1
        else:
            i += 1

        delta = elements[j] + elements[i] - required

    return pair


if __name__ == '__main__':
    a = [1, 4, 5, 7]
    b = [10, 20, 30, 40]
    print(find_closest_pair(a, b, 32))