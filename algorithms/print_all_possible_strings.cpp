#include <iostream>
#include <string>
#include <unordered_set>
using namespace std;


void all_possible_strings(string input, unordered_set<string> &output, int k)
{
    if (output.count(input) > 0) {
        return ;
    }

    output.insert(input);

    string temp;
    for (int i = k; i > 0; i--)
    {
        temp = input;
        temp.insert(i+1, " ");

        all_possible_strings(temp, output, i-1);
    }

    temp = input;
    temp.insert(1, " ");
    output.insert(temp);
}


int main()
{
    string input = "ABCD";
    unordered_set<string> output;

    int n = input.length();
    all_possible_strings(input, output, n-1);

    for (auto it = output.begin(); it != output.end(); ++it)
    {
        cout << *it << endl;
    }

    return 0;
}