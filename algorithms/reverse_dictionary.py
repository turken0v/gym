from typing import Dict


def reverse_dictionary(dataset: Dict) -> Dict:
    new = {}
    for key, value in dataset.items():
        busy = new.get(value, None)
        if busy is None:
            new[value] = [key]
        else:
            new[value] = busy + [key]

    return new


if __name__ == '__main__':
    routes = {
        'Chennai': 'Banglore',
        'Bombay': 'Delhi',
        'Goa': 'Chennai',
        'Delhi': 'Goa'
    }
    print(reverse_dictionary(routes))