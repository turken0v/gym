#include <bits/stdc++.h>
using namespace std;


int maximum_golds(vector<vector<int>> &matrix) {
    int n = matrix.size();

    int a, b, c;
    for (int i = 1; i < n; i++) {
        for (int j = 0; j < n; j++) {
            a = INT_MIN;
            if (j > 0) {
                a = matrix[j-1][i-1];
            }

            b = matrix[j][i-1];

            c = INT_MIN;
            if (n > j+1) {
                c = matrix[j+1][i-1];
            }

            matrix[j][i] += max(a, max(b, c));
        }
    }

    int answer = INT_MIN;
    for (int i = 0; i < n; i++) {
        answer = max(answer, matrix[i][n-1]);
    }

    return answer;
}


int main() {
    vector<vector<int>> matrix;

    matrix = {
        {1,3,1,5},
        {2,2,4,1},
        {5,0,2,3},
        {0,6,1,2},
    };
    cout << maximum_golds(matrix) << endl;

    return 0;
}