def three_sum(array) -> bool:
    size = len(array)
    for i in range(0, size):
        l, r = i+1, size-1
        while r > l:
            value = array[i] + array[l] + array[r]
            if value == 0:
                return True
            elif value > 0:
                r -= 1
            else:
                l += 1



if __name__ == '__main__':
    print(three_sum([-4,-1,0,1,2]))