from heapq import heapify, heappop as hpop


def prim():
    pass


def vertices_power(G):
    V = {}

    for e in G:
        v1, v2 = e
        m = V.get(v1, 0)
        V[v1] = m + 1

        m = V.get(v2, 0)
        V[v2] = m + 1

    return V


def is_cycled(G, goal):
    pass


def kruskal(V, E, W):
    n = len(E)
    if n != len(W):
        raise ValueError

    A = [(W[i], E[i]) for i in range(n)]
    heapify(A)

    G = []
    s = 0
    for _ in range(n):
        w, e = hpop(A)
        if not is_cycled(G + [e]):
            s += w
            G.append(e)

    return s, G


if __name__ == '__main__':
    V = set([1, 2, 3, 4, 5, 6, 7])
    E = [(1, 2), (2, 3), (2, 7), (3, 4), (4, 7), (4, 5), (5, 7), (5, 6), (6, 1)]
    W = [28, 16, 14, 12, 18, 22, 24, 25, 10]

    print(is_cycled(E))

    # print(kruskal(V, E, W))